﻿using System;
using System.Text;
using System.Windows;
using AutoComplete.Controls;

namespace AutoComplete.Demos
{
    partial class MainWindow
    {
        public MainWindow() => this.InitializeComponent();

        private void DisplayTextData_Click(object sender, RoutedEventArgs e)
        {
            StringBuilder builder = new StringBuilder();
            foreach (object datum in this.Multi.GetTextData())
            {
                switch (datum)
                {
                    case string str:
                        builder.AppendLine(str);
                        break;
                    case Chip chip:
                        builder.AppendLine($"Chip: {chip.KeyValue}");
                        break;
                }
            }
            MessageBox.Show(builder.ToString());
        }

        private void SetListControlText_Click(object sender, RoutedEventArgs e)
        {
            this.Multi.Text = "zky1hvm;garbage1;garbage2;NBKZOIS;garbage3;";
        }

        private void SetTypedListSelectedItemKeyButton_Click(object sender, RoutedEventArgs e)
        {
            this.TypedList.SelectedItemKey = "FM";
            if (this.TypedList.SelectedItem != null)
            {
                Console.WriteLine(((DataBoundObject)this.TypedList.SelectedItem)["Name"]);
            }
        }
    }
}