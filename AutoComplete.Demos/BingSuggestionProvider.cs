﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using AutoComplete.Controls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AutoComplete.Demos
{
    public sealed class BingSuggestionProvider : ISuggestionProvider
    {
        private static readonly JsonSerializer _serializer = new JsonSerializer();

        public BingSuggestionProvider()
            : this(1)
        {
        }

        public BingSuggestionProvider(int minCharacters) => this.MinCharacters = minCharacters;

        public int MinCharacters
        {
            get;
        }

        public async Task<IEnumerable<SuggestionData>> GetSuggestions(ISuggestionParameters parameters, string searchText, CancellationToken cancellationToken)
        {
            HttpWebRequest request = WebRequest.CreateHttp($"http://api.bing.com/osjson.aspx?query={WebUtility.UrlEncode(searchText)}");
            request.Credentials = CredentialCache.DefaultNetworkCredentials;
            if (request.Proxy != null)
            {
                request.Proxy.Credentials = CredentialCache.DefaultNetworkCredentials;
            }
            if (cancellationToken.IsCancellationRequested)
            {
                return Enumerable.Empty<SuggestionData>();
            }
            string json;
            using (HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync().ConfigureAwait(false))
            {
                if (cancellationToken.IsCancellationRequested || response.StatusCode != HttpStatusCode.OK)
                {
                    return Enumerable.Empty<SuggestionData>();
                }
                using Stream stream = response.GetResponseStream();
                if (stream == null)
                {
                    return Enumerable.Empty<SuggestionData>();
                }
                using TextReader streamReader = new StreamReader(stream);
                json = await streamReader.ReadToEndAsync().ConfigureAwait(false);
            }
            if (cancellationToken.IsCancellationRequested)
            {
                return Enumerable.Empty<SuggestionData>();
            }
            using TextReader stringReader = new StringReader(json);
            using JsonReader jsonReader = new JsonTextReader(stringReader);
            return (from token in BingSuggestionProvider._serializer.Deserialize<JArray>(jsonReader).Last().Take(parameters.MaxSuggestions)
                    select (string)token into text
                    let startIndex = text.IndexOf(searchText, StringComparison.OrdinalIgnoreCase)
                    select parameters.CreateSuggestionData(text, searchText) into datum
                    orderby datum.Cells[0].Segments.Count == 0 ? int.MaxValue : datum.Cells[0].Segments[0].StartIndex, (string)datum.Item
                    select datum).ToList();
        }
    }
}