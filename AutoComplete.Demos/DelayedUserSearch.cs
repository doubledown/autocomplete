﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoComplete.Controls;

namespace AutoComplete.Demos
{
    public static class DelayedUserSearch
    {
        public static readonly IKeySearchProvider DelayedKeySearchProvider = new KeySearchProvider();

        public static readonly ISuggestionProvider DelayedSuggestionProvider = new SuggestionProvider();

        private sealed class KeySearchProvider : IKeySearchProvider
        {
            public Task<object> FindItemByKey(IKeySearchParameters parameters, string key, CancellationToken cancellationToken)
            {
                return Task.Delay(250, cancellationToken).ContinueWith(
                    task =>
                    {
                        return (object)User.All.FirstOrDefault(user => string.Equals(parameters.ColumnDataProjection.GetTextColumnValue(user, parameters.KeyColumnName), key, StringComparison.OrdinalIgnoreCase));
                    },
                    cancellationToken,
                    TaskContinuationOptions.NotOnCanceled,
                    TaskScheduler.Current
                );
            }
        }

        private sealed class SuggestionProvider : ISuggestionProvider
        {
            public int MinCharacters => 1;

            public Task<IEnumerable<SuggestionData>> GetSuggestions(ISuggestionParameters parameters, string searchText, CancellationToken cancellationToken)
            {
                return Task.Delay(500, cancellationToken).ContinueWith(
                    task =>
                    {
                        List<SuggestionData> list = new List<SuggestionData>();
                        foreach (SuggestionData? data in User.All.Select(user => parameters.TryCreateSuggestionData(user, searchText)).Where(data => data.HasValue))
                        {
                            list.Add(data.Value);
                            if (list.Count >= parameters.MaxSuggestions)
                            {
                                break;
                            }
                        }
                        return (IEnumerable<SuggestionData>)list;
                    },
                    cancellationToken,
                    TaskContinuationOptions.NotOnCanceled | TaskContinuationOptions.ExecuteSynchronously,
                    TaskScheduler.Current
                );
            }
        }
    }
}