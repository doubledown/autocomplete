﻿using System;
using System.Collections.ObjectModel;

namespace AutoComplete.Demos
{
    public readonly struct User
    {
        public static ReadOnlyCollection<User> All = Array.AsReadOnly(new[]
        {
            new User("NBDKXKD", "Lai, Nick", "Contractor", "nick.lai@baml.com"),
            new User("NBDWTOX", "Cobo, Luis A", "Vice President", "luis.cobo@baml.com"),
            new User("NBKDCC4", "Leith, Liz", "Administrative Assistant III", "liz2.leith@baml.com"),
            new User("NBKJLAO", "Boswell, Dan", "Director", "daniel.d.boswell@baml.com"),
            new User("NBKQ0K9", "Harrison, Ben", "Director", "ben.harrison@baml.com"),
            new User("NBKS6CA", "Hsu, Fred", "Director", "fred.hsu@baml.com"),
            new User("NBKV4W0", "Pradhan, Surya", "Vice President", "surya.pradhan@baml.com"),
            new User("NBKZOIS", "Baumslag, Marc", "Managing Director", "marc.baumslag@baml.com"),
            new User("NBKAH10", "Madasu, Anand", "Lead Analyst", "anand.madasu@bankofamerica.com"),
            new User("ZK1LN0D", "Zhou, Mia", "Assistant Vice President", "mia.zhou@baml.com"),
            new User("ZK2HY17", "Matyakubov, Bahrom", "Assistant Vice President", "bahrom.matyakubov@baml.com"),
            new User("ZK3MJB2", "Tasdemir, Cagil", "Officer", "cagil.tasdemir@baml.com"),
            new User("ZKF1NMP", "Gadi, Hitesh", "Vice President", "hitesh.gadi@baml.com"),
            new User("ZKFKRZS", "Milligan, Eoin", "Vice President", "eoin.p.milligan@baml.com"),
            new User("ZKJJGCB", "Costa Matos, Margarita", "Vice President", "margarita.costa_matos@baml.com"),
            new User("ZKY1HVM", "Burbea, Amir", "Vice President", "amir.burbea@baml.com")
        });

        private User(string nbkId, string name, string title, string email)
        {
            this.NbkId = nbkId;
            this.Name = name;
            this.Title = title;
            this.Email = email;
        }

        public string Email
        {
            get;
        }

        public string Name
        {
            get;
        }

        public string NbkId
        {
            get;
        }

        public string Title
        {
            get;
        }
    }
}