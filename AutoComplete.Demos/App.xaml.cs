﻿using System;
using System.IO;

namespace AutoComplete.Demos
{
    partial class App
    {
        private App()
        {
            AppDomain.CurrentDomain.UnhandledException += App.CurrentDomain_UnhandledException;
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), e.ExceptionObject.ToString());
        }
    }
}