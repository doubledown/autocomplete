﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;

namespace AutoComplete.Demos
{
    public sealed class State
    {
        public static readonly ReadOnlyCollection<State> All = State.CreateReadOnlyCollection(new[]
        {
            new State("Alabama", "AL"),
            new State("Alaska", "AK"),
            new State("American Samoa", "AS"),
            new State("Arizona", "AZ"),
            new State("Arkansas", "AR"),
            new State("California", "CA"),
            new State("Colorado", "CO"),
            new State("Connecticut", "CT"),
            new State("Delaware", "DE"),
            new State("District of Columbia", "DC"),
            new State("Federated States of Micronesia", "FM"),
            new State("Florida", "FL"),
            new State("Georgia", "GA"),
            new State("Guam", "GU"),
            new State("Hawaii", "HI"),
            new State("Idaho", "ID"),
            new State("Illinois", "IL"),
            new State("Indiana", "IN"),
            new State("Iowa", "IA"),
            new State("Kansas", "KS"),
            new State("Kentucky", "KY"),
            new State("Louisiana", "LA"),
            new State("Maine", "ME"),
            new State("Marshall Islands", "MH"),
            new State("Maryland", "MD"),
            new State("Massachusetts", "MA"),
            new State("Michigan", "MI"),
            new State("Minnesota", "MN"),
            new State("Mississippi", "MS"),
            new State("Missouri", "MO"),
            new State("Montana", "MT"),
            new State("Nebraska", "NE"),
            new State("Nevada", "NV"),
            new State("New Hampshire", "NH"),
            new State("New Jersey", "NJ"),
            new State("New Mexico", "NM"),
            new State("New York", "NY"),
            new State("North Carolina", "NC"),
            new State("North Dakota", "ND"),
            new State("Northern Mariana Islands", "MP"),
            new State("Ohio", "OH"),
            new State("Oklahoma", "OK"),
            new State("Oregon", "OR"),
            new State("Palau", "PW"),
            new State("Pennsylvania", "PA"),
            new State("Puerto Rico", "PR"),
            new State("Rhode Island", "RI"),
            new State("South Carolina", "SC"),
            new State("South Dakota", "SD"),
            new State("Tennessee", "TN"),
            new State("Texas", "TX"),
            new State("Utah", "UT"),
            new State("Vermont", "VT"),
            new State("Virgin Islands", "VI"),
            new State("Virginia", "VA"),
            new State("Washington", "WA"),
            new State("West Virginia", "WV"),
            new State("Wisconsin", "WI"),
            new State("Wyoming", "WY")
        });

        public static readonly DataTable DataTable = State.CreateDataTable();

        public static readonly ReadOnlyCollection<string> Names = State.All.Select(state => state.Name).ToList().AsReadOnly();

        public static readonly DataBoundObjectCollection TypedList = new DataBoundObjectCollection(State.DataTable);

        private State(string name, string code)
        {
            this.Name = name;
            this.Code = code;
        }

        public string Code
        {
            get;
        }

        public string Name
        {
            get;
        }

        public int Ordinal
        {
            get;
            private set;
        }

        private static DataTable CreateDataTable()
        {
            DataTable dataTable = new DataTable
            {
                Columns =
                {
                    { nameof(State.Name), typeof(string) },
                    { nameof(State.Code), typeof(string) },
                    { nameof(State.Ordinal), typeof(int) }
                }
            };
            object[] values = new object[3];
            foreach (State state in State.All)
            {
                values[0] = state.Name;
                values[1] = state.Code;
                values[2] = state.Ordinal;
                dataTable.Rows.Add(values);
            }
            dataTable.AcceptChanges();
            return dataTable;
        }

        private static ReadOnlyCollection<State> CreateReadOnlyCollection(State[] states)
        {
            for (int index = 0; index < states.Length; index++)
            {
                states[index].Ordinal = index;
            }
            return Array.AsReadOnly(states);
        }
    }
}