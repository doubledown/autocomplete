using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

namespace AutoComplete.Demos
{
    public sealed class DataBoundObjectCollection : List<DataBoundObject>, ITypedList
    {
        public DataBoundObjectCollection(DataTable dataTable)
        {
            PropertyDescriptor[] properties = new PropertyDescriptor[dataTable.Columns.Count];
            for (int index = 0; index < dataTable.Columns.Count; index++)
            {
                DataColumn column = dataTable.Columns[index];
                properties[index] = new ColumnPropertyDescriptor(
                    column.ColumnName,
                    index,
                    !column.AllowDBNull || !column.DataType.IsValueType ? column.DataType : typeof(Nullable<>).MakeGenericType(column.DataType)
                );
            }
            this.Properties = new PropertyDescriptorCollection(properties, true);
            foreach (DataRow row in dataTable.Rows)
            {
                this.Add(new DataBoundObject(this, row));
            }
            this.CustomTypeDescriptor = new DataBoundTypeDescriptor(this.Properties);
        }

        public ICustomTypeDescriptor CustomTypeDescriptor
        {
            get;
        }

        public PropertyDescriptorCollection Properties
        {
            get;
        }

        public void Dispose()
        {
        }

        PropertyDescriptorCollection ITypedList.GetItemProperties(PropertyDescriptor[] listAccessors)
        {
            return this.Properties;
        }

        string ITypedList.GetListName(PropertyDescriptor[] listAccessors)
        {
            return null;
        }

        private sealed class ColumnPropertyDescriptor : PropertyDescriptor
        {
            private readonly int _ordinal;

            public ColumnPropertyDescriptor(string name, int ordinal, Type propertyType)
                : base(name, null)
            {
                this._ordinal = ordinal;
                this.PropertyType = propertyType;
            }

            public override Type ComponentType => typeof(DataBoundObject);

            public override bool IsReadOnly => true;

            public override Type PropertyType
            {
                get;
            }

            public override bool CanResetValue(object component)
            {
                return false;
            }

            public override object GetValue(object component)
            {
                return ((DataBoundObject)component).GetValue(this._ordinal);
            }

            public override void ResetValue(object component)
            {
                throw new NotSupportedException();
            }

            public override void SetValue(object component, object value)
            {
                throw new NotSupportedException();
            }

            public override bool ShouldSerializeValue(object component)
            {
                return false;
            }
        }

        private sealed class DataBoundTypeDescriptor : CustomTypeDescriptor
        {
            private readonly PropertyDescriptorCollection _properties;

            public DataBoundTypeDescriptor(PropertyDescriptorCollection properties)
            {
                this._properties = properties;
            }

            public override PropertyDescriptorCollection GetProperties()
            {
                return this._properties;
            }

            public override PropertyDescriptorCollection GetProperties(Attribute[] attributes)
            {
                return this._properties;
            }
        }
    }
}