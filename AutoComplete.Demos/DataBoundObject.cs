﻿using System;
using System.ComponentModel;
using System.Data;

namespace AutoComplete.Demos
{
    [TypeDescriptionProvider(typeof(DataBoundTypeDescriptionProvider))]
    public sealed class DataBoundObject
    {
        private readonly DataBoundObjectCollection _collection;
        private readonly DataRow _dataRow;

        internal DataBoundObject(DataBoundObjectCollection collection, DataRow dataRow)
        {
            this._collection = collection;
            this._dataRow = dataRow;
        }

        public object this[string columnName] => this._dataRow[columnName];

        public object GetValue(int ordinal) => this._dataRow[ordinal];

        private sealed class DataBoundTypeDescriptionProvider : TypeDescriptionProvider
        {
            public override ICustomTypeDescriptor GetTypeDescriptor(Type objectType, object instance)
            {
                return instance == null ? base.GetTypeDescriptor(objectType, null) : ((DataBoundObject)instance)._collection.CustomTypeDescriptor;
            }
        }
    }
}