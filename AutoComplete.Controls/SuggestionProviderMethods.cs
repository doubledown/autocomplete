﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace AutoComplete.Controls
{
    public delegate void AsyncSuggestionProjection(IAsyncSuggestionOperation operation);

    public delegate IEnumerable<SuggestionData> SuggestionProjection(ISuggestionParameters parameters, string searchText);

    public interface IAsyncSuggestionOperation : IAsyncOperation<ISuggestionParameters, IEnumerable<SuggestionData>>
    {
    }

    public static class SuggestionProviderMethods
    {
        public static ISuggestionProvider CreateAsyncProvider(this AsyncSuggestionProjection projection, int minCharacters = 1)
        {
            if (minCharacters < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(minCharacters));
            }
            return new AsyncDelegateSuggestionProvider(projection ?? throw new ArgumentNullException(nameof(projection)), minCharacters);
        }

        public static ISuggestionProvider CreateProvider(this SuggestionProjection projection, int minCharacters = 1)
        {
            if (minCharacters < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(minCharacters));
            }
            return new DelegateSuggestionProvider(projection ?? throw new ArgumentNullException(nameof(projection)), minCharacters);
        }

        private sealed class AsyncDelegateSuggestionProvider : ISuggestionProvider
        {
            private readonly AsyncSuggestionProjection _projection;

            public AsyncDelegateSuggestionProvider(AsyncSuggestionProjection projection, int minCharacters)
            {
                this._projection = projection;
                this.MinCharacters = minCharacters;
            }

            public int MinCharacters
            {
                get;
            }

            public Task<IEnumerable<SuggestionData>> GetSuggestions(ISuggestionParameters parameters, string searchText, CancellationToken cancellationToken)
            {
                AsyncSuggestionOperation operation = new AsyncSuggestionOperation(parameters, searchText, cancellationToken);
                this._projection.Invoke(operation);
                return operation.Task;
            }

            private sealed class AsyncSuggestionOperation : AsyncOperationBase<ISuggestionParameters, IEnumerable<SuggestionData>>, IAsyncSuggestionOperation
            {
                public AsyncSuggestionOperation(ISuggestionParameters parameters, string searchText, CancellationToken cancellationToken, object state = null)
                    : base(parameters, searchText, cancellationToken, state)
                {
                }
            }
        }

        private sealed class DelegateSuggestionProvider : ISuggestionProvider
        {
            private readonly SuggestionProjection _projection;

            public DelegateSuggestionProvider(SuggestionProjection projection, int minCharacters)
            {
                this._projection = projection;
                this.MinCharacters = minCharacters;
            }

            public int MinCharacters
            {
                get;
            }

            public Task<IEnumerable<SuggestionData>> GetSuggestions(ISuggestionParameters parameters, string searchText, CancellationToken cancellationToken)
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    return Task.FromCanceled<IEnumerable<SuggestionData>>(cancellationToken);
                }
                try
                {
                    return Task.FromResult(this._projection(parameters, searchText));
                }
                catch (Exception e)
                {
                    return Task.FromException<IEnumerable<SuggestionData>>(e);
                }
            }
        }
    }
}