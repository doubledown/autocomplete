﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace AutoComplete.Controls
{
    public delegate void AsyncKeySearchProjection(IAsyncKeySearchOperation operation);

    public delegate object KeySearchProjection(IKeySearchParameters parameters, string searchText);

    public interface IAsyncKeySearchOperation : IAsyncOperation<IKeySearchParameters, object>
    {
    }

    public static class KeySearchProviderMethods
    {
        public static IKeySearchProvider CreateAsyncProvider(this AsyncKeySearchProjection projection)
        {
            return new AsyncDelegateKeySearchProvider(projection ?? throw new ArgumentNullException(nameof(projection)));
        }

        public static IKeySearchProvider CreateProvider(this KeySearchProjection projection)
        {
            return new DelegateKeySearchProvider(projection ?? throw new ArgumentNullException(nameof(projection)));
        }

        private sealed class AsyncDelegateKeySearchProvider : IKeySearchProvider
        {
            private readonly AsyncKeySearchProjection _projection;

            public AsyncDelegateKeySearchProvider(AsyncKeySearchProjection projection) => this._projection = projection;

            public Task<object> FindItemByKey(IKeySearchParameters parameters, string searchText, CancellationToken cancellationToken)
            {
                AsyncKeySearchOperation operation = new AsyncKeySearchOperation(parameters, searchText, cancellationToken);
                this._projection.Invoke(operation);
                return operation.Task;
            }

            private sealed class AsyncKeySearchOperation : AsyncOperationBase<IKeySearchParameters, object>, IAsyncKeySearchOperation
            {
                public AsyncKeySearchOperation(IKeySearchParameters parameters, string searchText, CancellationToken cancellationToken, object state = null)
                    : base(parameters, searchText, cancellationToken, state)
                {
                }
            }
        }

        private sealed class DelegateKeySearchProvider : IKeySearchProvider
        {
            private readonly KeySearchProjection _projection;

            public DelegateKeySearchProvider(KeySearchProjection projection) => this._projection = projection;

            public Task<object> FindItemByKey(IKeySearchParameters parameters, string searchText, CancellationToken cancellationToken)
            {
                TaskCompletionSource<object> source = new TaskCompletionSource<object>();
                try
                {
                    if (cancellationToken.IsCancellationRequested)
                    {
                        source.TrySetCanceled();
                    }
                    else
                    {
                        source.TrySetResult(this._projection.Invoke(parameters, searchText));
                    }
                }
                catch (Exception e)
                {
                    source.TrySetException(e);
                }
                return source.Task;
            }
        }
    }
}