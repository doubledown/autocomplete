﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace AutoComplete.Controls
{
    public sealed class IsSelfReferencingColumnNameConverter : IValueConverter
    {
        object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string columnName = value as string;
            return BooleanBoxes.GetBox(string.IsNullOrEmpty(columnName) || columnName == ".");
        }

        object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}