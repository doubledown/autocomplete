﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AutoComplete.Controls
{
    public sealed class ItemsSourceSuggestionProvider : ISuggestionProvider
    {
        public ItemsSourceSuggestionProvider()
            : this(1)
        {
        }

        public ItemsSourceSuggestionProvider(int minCharacters)
        {
            this.MinCharacters = minCharacters < 1 ? 1 : minCharacters;
        }

        public int MinCharacters
        {
            get;
        }

        public Task<IEnumerable<SuggestionData>> GetSuggestions(ISuggestionParameters parameters, string searchText, CancellationToken cancellationToken)
        {
            TaskCompletionSource<IEnumerable<SuggestionData>> source = new TaskCompletionSource<IEnumerable<SuggestionData>>();
            try
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    source.TrySetCanceled();
                }
                else if (parameters.ItemsSource == null || string.IsNullOrEmpty(searchText = searchText?.Trim()))
                {
                    source.TrySetResult(null);
                }
                else
                {
                    List<SuggestionData> list = parameters.ItemsSource
                        .Cast<object>()
                        .Select(item => parameters.TryCreateSuggestionData(item, searchText))
                        .Where(datum => datum.HasValue)
                        .Select(datum => datum.Value)
                        .Take(parameters.MaxSuggestions)
                        .OrderBy(datum => datum, SuggestionDataComparer.Instance)
                        .ToList();
                    source.TrySetResult(list.Count == 0 ? null : list);
                }
            }
            catch (Exception e)
            {
                source.TrySetException(e);
            }
            return source.Task;
        }

        private sealed class SuggestionDataComparer : IComparer<SuggestionData>
        {
            public static readonly SuggestionDataComparer Instance = new SuggestionDataComparer();

            int IComparer<SuggestionData>.Compare(SuggestionData x, SuggestionData y)
            {
                for (int index = 0; index < x.Cells.Count; index++)
                {
                    SuggestionCell xCell = x.Cells[index];
                    SuggestionCell yCell = y.Cells[index];
                    bool xHasMatches = xCell.Segments.Count != 0;
                    bool yHasMatches = yCell.Segments.Count != 0;
                    if (!xHasMatches && !yHasMatches)
                    {
                        continue;
                    }
                    if (!xHasMatches)
                    {
                        return 1;
                    }
                    if (!yHasMatches)
                    {
                        return -1;
                    }
                    if (xCell.Segments[0].StartIndex != yCell.Segments[0].StartIndex)
                    {
                        return xCell.Segments[0].StartIndex.CompareTo(yCell.Segments[0].StartIndex);
                    }
                }
                // Compare cell values where matched;
                for (int index = 0; index < x.Cells.Count; index++)
                {
                    SuggestionCell xCell = x.Cells[index];
                    SuggestionCell yCell = y.Cells[index];
                    if ((xCell.Segments.Count != 0 || yCell.Segments.Count != 0) && xCell.Value != yCell.Value)
                    {
                        return StringComparer.OrdinalIgnoreCase.Compare(xCell.Value, yCell.Value);
                    }
                }
                return 0;
            }
        }
    }
}