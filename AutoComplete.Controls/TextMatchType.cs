﻿namespace AutoComplete.Controls
{
    /// <summary>
    /// Enumeration specifying how to match text.
    /// </summary>
    public enum TextMatchType
    {
        /// <summary>
        /// Match text starting with the search text.
        /// </summary>
        StartsWith = 0, 

        /// <summary>
        /// Match text containing the search text anywhere.
        /// </summary>
        Contains,

        /// <summary>
        /// Match text ending with the search text.
        /// </summary>
        EndsWith,
    }
}