﻿using System;

namespace AutoComplete.Controls
{
    public readonly struct Chip : IEquatable<Chip>
    {
        public Chip(object item, string displayValue, string keyValue)
        {
            this.KeyValue = keyValue;
            this.Item = item;
            this.DisplayValue = displayValue;
        }

        public string DisplayValue
        {
            get;
        }

        public object Item
        {
            get;
        }

        public string KeyValue
        {
            get;
        }

        public static bool operator !=(Chip left, Chip right)
        {
            return !left.Equals(right);
        }

        public static bool operator ==(Chip left, Chip right)
        {
            return left.Equals(right);
        }

        public bool Equals(Chip other)
        {
            return object.Equals(this.Item, other.Item) && string.Equals(this.DisplayValue, other.DisplayValue) && string.Equals(this.KeyValue, other.KeyValue);
        }

        public override bool Equals(object obj) => obj is Chip chip && this.Equals(chip);

        public override int GetHashCode()
        {
            if (this.Item == null)
            {
                return 0;
            }
            unchecked
            {
                int hashCode = this.Item.GetHashCode();
                if (this.DisplayValue != null)
                {
                    hashCode = hashCode * 397 ^ this.DisplayValue.GetHashCode();
                }
                if (this.KeyValue != null)
                {
                    hashCode = hashCode * 397 ^ this.KeyValue.GetHashCode();
                }
                return hashCode;
            }
        }
    }
}