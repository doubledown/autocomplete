﻿using System;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace AutoComplete.Controls
{
    public sealed class AutoCompleteControl : AutoCompleteControlBase
    {
        public static readonly RoutedEvent SelectedItemChangedEvent = EventManager.RegisterRoutedEvent("SelectedItemChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(AutoCompleteControl));

        public static readonly DependencyProperty SelectedItemKeyProperty = DependencyProperty.Register(nameof(AutoCompleteControl.SelectedItemKey), typeof(string), typeof(AutoCompleteControl),
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, AutoCompleteControl.SelectedItemKey_PropertyChanged));

        public static readonly DependencyProperty SelectedItemProperty = DependencyProperty.Register(nameof(AutoCompleteControl.SelectedItem), typeof(object), typeof(AutoCompleteControl),
            new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, AutoCompleteControl.SelectedItem_PropertyChanged));

        private readonly NestableScopeCreator _suppressSelectedItemKeyChangedScopeCreator = new NestableScopeCreator();
        private TextBox _textBox;

        static AutoCompleteControl()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(AutoCompleteControl), new FrameworkPropertyMetadata(typeof(AutoCompleteControl)));
        }

        public event RoutedEventHandler SelectedItemChanged
        {
            add => this.AddHandler(AutoCompleteControl.SelectedItemChangedEvent, value);
            remove => this.RemoveHandler(AutoCompleteControl.SelectedItemChangedEvent, value);
        }

        public object SelectedItem
        {
            get => this.GetValue(AutoCompleteControl.SelectedItemProperty);
            set => this.SetValue(AutoCompleteControl.SelectedItemProperty, value);
        }

        public string SelectedItemKey
        {
            get => (string)this.GetValue(AutoCompleteControl.SelectedItemKeyProperty);
            set => this.SetValue(AutoCompleteControl.SelectedItemKeyProperty, value);
        }

        private bool IsSuppressingSelectedItemKeyChanged => this._suppressSelectedItemKeyChangedScopeCreator.HasActiveScope;

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            if ((this._textBox = this.TextBox as TextBox) == null)
            {
                throw new InvalidOperationException("Attempt to apply template without required template parts");
            }
        }

        protected override string GetSuggestionSearchText() => this.Text;

        protected override void OnDisplayColumnNameChanged(string newValue)
        {
            base.OnDisplayColumnNameChanged(newValue);
            if (this.SelectedItem == null)
            {
                return;
            }
            using (this.SuppressTextChanged())
            {
                this.Text = this.GetTextColumnValue(this.SelectedItem, newValue);
            }
        }

        protected override void OnItemSelected(object selectedItem)
        {
            base.OnItemSelected(selectedItem);
            if (selectedItem != null)
            {
                this.SelectedItem = selectedItem;
            }
        }

        protected override void OnTextChanged(string newValue)
        {
            base.OnTextChanged(newValue);
            if (this.IsSuppressingTextChanged)
            {
                return;
            }
            this.SelectedItem = null;
            if (!this.IsSuppressingSuggestions && !string.IsNullOrEmpty(newValue) && newValue.Length >= this.SuggestionProvider.MinCharacters)
            {
                this.SearchForSuggestions();
            }
        }

        protected override void ResetListViewSelection()
        {
            this.ListView.SelectedItem = this.SelectedItem;
        }

        protected override void SetFocusToTextBox()
        {
            if (this._textBox == null)
            {
                return;
            }
            base.SetFocusToTextBox();
            this.Dispatcher.InvokeAsync(() =>
            {
                this._textBox.Focus();
                this._textBox.CaretIndex = this._textBox.Text.Length;
            }, DispatcherPriority.Render);
        }

        private static void SelectedItem_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((AutoCompleteControl)d).OnSelectedItemChanged(e.NewValue);
        }

        private static void SelectedItemKey_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((AutoCompleteControl)d).OnSelectedItemKeyChanged((string)e.NewValue);
        }

        private void OnSelectedItemChanged(object newValue)
        {
            using (this.SuppressSelectedItemKeyChanged())
            {
                this.SelectedItemKey = newValue == null ? null : this.GetTextColumnValue(newValue, this.KeyColumnName);
            }
            if (newValue != null)
            {
                using (this.SuppressSuggestions())
                {
                    using (this.SuppressTextChanged())
                    {
                        this.Text = this.GetTextColumnValue(newValue, this.DisplayColumnName);
                    }
                }
                this.SetFocusToTextBox();
            }
            this.RaiseEvent(new RoutedEventArgs(AutoCompleteControl.SelectedItemChangedEvent));
        }

        private void OnSelectedItemKeyChanged(string newValue)
        {
            if (this.IsSuppressingSelectedItemKeyChanged)
            {
                return;
            }
            if (newValue == null)
            {
                this.SelectedItem = null;
                return;
            }
            this.SearchByKey(newValue).ContinueWith(
                task =>
                {
                    // Processing the effects of the task can only be done on the UI thread, but we do not want to use TaskScheduler.FromCurrentSynchronizationContext()
                    // as it internally would use Dispatcher.BeginInvoke, which means if the SelectedItem could be available now as opposed to via callback, that would be preferred.
                    if (this.Dispatcher.CheckAccess())
                    {
                        this.ProcessCompletedSearchByKeyTask(task);
                    }
                    else
                    {
                        this.Dispatcher.Invoke(DispatcherPriority.Background, new Action<Task<object>>(this.ProcessCompletedSearchByKeyTask), task);
                    }
                },
                TaskContinuationOptions.ExecuteSynchronously
            );
        }

        /// <summary>
        /// Search by key can only be continued on the GUI thread, but if the search operation was on the GUI thread we want to execute this immediately.
        /// Unfortunately using TaskScheduler.FromCurrentSynchronizationContext() would call Dispatcher.BeginInvoke, which means the SelectedItemKey would not be set at the same time.
        /// </summary>
        /// <param name="task"></param>
        private void ProcessCompletedSearchByKeyTask(Task<object> task)
        {
            if (!task.IsFaulted && task.Result != null)
            {
                this.SelectedItem = task.Result;
            }
            else
            {
                // Clear the invalid value.
                this.SelectedItemKey = null;
            }
        }

        private IDisposable SuppressSelectedItemKeyChanged() => this._suppressSelectedItemKeyChangedScopeCreator.CreateScope();
    }
}
