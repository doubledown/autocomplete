﻿using System;
using System.Threading;

namespace AutoComplete.Controls
{
    /// <summary>
    /// A class that can create <see cref="T:System.IDisposable"/> scope objects.  As long as one created scope object is not disposed
    /// the <code>HasActiveScope</code> property will return <code>true</code>.  Optionally, the class can call an <see cref="T:System.Action" /> when
    /// the first active scope is created and/or an <see cref="T:System.Action" /> when the last active scope is disposed.
    /// </summary>
    internal sealed class NestableScopeCreator
    {
        private readonly Action _firstScopeCreatedAction;
        private readonly Action _lastScopeDisposedAction;
        private int _counter;

        /// <summary>
        /// Initializes a new instance of the <see cref="NestableScopeCreator"/> class.
        /// </summary>
        /// <param name="firstScopeCreatedAction">The first scope created action. (This parameter can be left <c>null</c>).</param>
        /// <param name="lastScopeDisposedAction">The last scope disposed action. (This parameter can be left <c>null</c>).</param>
        public NestableScopeCreator(Action firstScopeCreatedAction = null, Action lastScopeDisposedAction = null)
        {
            this._firstScopeCreatedAction = firstScopeCreatedAction;
            this._lastScopeDisposedAction = lastScopeDisposedAction;
        }

        /// <summary>
        /// Gets a value indicating whether this instance has at least one active scope that has not been disposed.
        /// </summary>
        /// <value><c>true</c> if this instance has at least one active scope that has not been disposed; otherwise, <c>false</c>.</value>
        public bool HasActiveScope => this._counter != 0;

        /// <summary>
        /// Creates an <see cref="T:System.IDisposable"/> scope object.
        /// </summary>
        /// <returns>IDisposable.</returns>
        public IDisposable CreateScope()
        {
            this.IncrementCounter();
            return new Disposable(this.DecrementCounter);
        }

        /// <summary>
        /// Decrements the counter.
        /// </summary>
        private void DecrementCounter()
        {
            if (this._counter != 0 && Interlocked.Decrement(ref this._counter) == 0)
            {
                this._lastScopeDisposedAction?.Invoke();
            }
        }

        /// <summary>
        /// Increments the counter.
        /// </summary>
        private void IncrementCounter()
        {
            if (this._counter != int.MaxValue && Interlocked.Increment(ref this._counter) == 1)
            {
                this._firstScopeCreatedAction?.Invoke();
            }
        }
    }
}