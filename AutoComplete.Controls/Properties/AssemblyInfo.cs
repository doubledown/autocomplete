﻿using System;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Markup;
 
[assembly: CLSCompliant(true)]
[assembly: ComVisible(false)]
[assembly: ThemeInfo(ResourceDictionaryLocation.None, ResourceDictionaryLocation.SourceAssembly)]

[assembly: XmlnsDefinition("http://schemas.amirburbea.com/autocomplete", "AutoComplete.Controls")]
[assembly: XmlnsPrefix("ac", "AutoComplete.Controls")]