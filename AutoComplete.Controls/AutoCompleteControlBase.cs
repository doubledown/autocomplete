﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace AutoComplete.Controls
{
    [TemplatePart(Name = "PART_TextBox", Type = typeof(TextBoxBase))]
    [TemplatePart(Name = "PART_ListView", Type = typeof(ListView))]
    [TemplatePart(Name = "PART_SuggestionsPopup", Type = typeof(Popup))]
    [TemplatePart(Name = "PART_SuggestionsItemsControl", Type = typeof(SuggestionItemsControl))]
    public abstract class AutoCompleteControlBase : Control
    {
        public static readonly DependencyProperty ColumnDataProviderProperty = DependencyProperty.Register(nameof(AutoCompleteControlBase.ColumnDataProvider), typeof(IColumnDataProvider), typeof(AutoCompleteControlBase),
            new PropertyMetadata(PropertyColumnDataProvider.Instance, AutoCompleteControlBase.ColumnDataProvider_PropertyChanged, AutoCompleteControlBase.CoerceColumnDataProvider));

        public static readonly DependencyProperty ColumnNamesProperty;

        public static readonly DependencyProperty DisplayColumnNameProperty = DependencyProperty.Register(nameof(AutoCompleteControlBase.DisplayColumnName), typeof(string), typeof(AutoCompleteControlBase),
            new PropertyMetadata(AutoCompleteControlBase.DisplayColumnName_PropertyChanged));

        public static readonly DependencyProperty GridViewProperty;

        public static readonly DependencyProperty IsBusyProperty;

        public static readonly DependencyProperty IsDropDownOpenProperty = DependencyProperty.Register(nameof(AutoCompleteControlBase.IsDropDownOpen), typeof(bool), typeof(AutoCompleteControlBase),
            new FrameworkPropertyMetadata(BooleanBoxes.FalseBox, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, AutoCompleteControlBase.IsDropDownOpen_PropertyChanged, AutoCompleteControlBase.CoerceIsDropDownOpen));

        public static readonly DependencyProperty IsReadOnlyProperty = DependencyProperty.Register(nameof(AutoCompleteControlBase.IsReadOnly), typeof(bool), typeof(AutoCompleteControlBase),
            new PropertyMetadata(BooleanBoxes.FalseBox));

        public static readonly DependencyProperty ItemsSourceProperty = DependencyProperty.Register(nameof(AutoCompleteControlBase.ItemsSource), typeof(IEnumerable), typeof(AutoCompleteControlBase),
            new PropertyMetadata(AutoCompleteControlBase.ItemsSource_PropertyChanged));

        public static readonly DependencyProperty KeyColumnNameProperty = DependencyProperty.Register(nameof(AutoCompleteControlBase.KeyColumnName), typeof(string), typeof(AutoCompleteControlBase),
            new PropertyMetadata(null, AutoCompleteControlBase.KeyColumnName_PropertyChanged));

        public static readonly DependencyProperty KeySearchProviderProperty = DependencyProperty.Register(nameof(AutoCompleteControlBase.KeySearchProvider), typeof(IKeySearchProvider), typeof(AutoCompleteControlBase),
            new PropertyMetadata(ItemsSourceKeySearchProvider.Instance, AutoCompleteControlBase.KeySearchProvider_PropertyChanged, AutoCompleteControlBase.CoerceKeySearchProvider));

        public static readonly DependencyProperty MaxSuggestionsProperty = DependencyProperty.Register(nameof(AutoCompleteControlBase.MaxSuggestions), typeof(int), typeof(AutoCompleteControlBase),
            new PropertyMetadata(25, AutoCompleteControlBase.MaxSuggestions_PropertyChanged, AutoCompleteControlBase.CoerceMaxSuggestions));

        public static readonly DependencyProperty MultiWordMatchTypeProperty = DependencyProperty.Register(nameof(AutoCompleteControlBase.MultiWordMatchType), typeof(MultiWordMatchType), typeof(AutoCompleteControlBase),
            new PropertyMetadata(AutoCompleteControlBase.MultiWordMatchType_PropertyChanged));

        public static readonly DependencyProperty ShowColumnHeadersProperty = DependencyProperty.Register(nameof(AutoCompleteControlBase.ShowColumnHeaders), typeof(bool), typeof(AutoCompleteControlBase),
            new PropertyMetadata(BooleanBoxes.FalseBox));

        public static readonly DependencyProperty SuggestionHeadersTemplateProperty;

        public static readonly DependencyProperty SuggestionProviderProperty = DependencyProperty.Register(nameof(AutoCompleteControlBase.SuggestionProvider), typeof(ISuggestionProvider), typeof(AutoCompleteControlBase),
            new PropertyMetadata(new ItemsSourceSuggestionProvider(), AutoCompleteControlBase.SuggestionProvider_PropertyChanged, AutoCompleteControlBase.CoerceSuggestionProvider));

        public static readonly DependencyProperty SuggestionsBackgroundProperty = DependencyProperty.Register(nameof(AutoCompleteControlBase.SuggestionsBackground), typeof(Brush), typeof(AutoCompleteControlBase),
            new PropertyMetadata());

        public static readonly DependencyProperty SuggestionsProperty;

        public static readonly DependencyProperty SuggestionTemplateProperty;

        public static readonly RoutedEvent TextChangedEvent = EventManager.RegisterRoutedEvent("TextChanged", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(AutoCompleteControlBase));

        public static readonly DependencyProperty TextMatchTypeProperty = DependencyProperty.Register(nameof(AutoCompleteControlBase.TextMatchType), typeof(TextMatchType), typeof(AutoCompleteControlBase),
            new PropertyMetadata(AutoCompleteControlBase.TextMatchType_PropertyChanged));

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register(nameof(AutoCompleteControlBase.Text), typeof(string), typeof(AutoCompleteControlBase),
            new FrameworkPropertyMetadata(string.Empty, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, AutoCompleteControlBase.Text_PropertyChanged));

        private static readonly DependencyPropertyKey _columnNamesPropertyKey = DependencyProperty.RegisterReadOnly(nameof(AutoCompleteControlBase.ColumnNames), typeof(ObservableCollection<string>), typeof(AutoCompleteControlBase),
            new PropertyMetadata());

        private static readonly string[] _defaultColumnNames = { "." };

        private static readonly DependencyPropertyKey _gridViewPropertyKey = DependencyProperty.RegisterReadOnly(nameof(AutoCompleteControlBase.GridView), typeof(GridView), typeof(AutoCompleteControlBase),
            new PropertyMetadata());

        private static readonly DependencyPropertyKey _isBusyPropertyKey = DependencyProperty.RegisterReadOnly(nameof(AutoCompleteControlBase.IsBusy), typeof(bool), typeof(AutoCompleteControlBase),
            new PropertyMetadata(BooleanBoxes.FalseBox));

        private static readonly DependencyPropertyKey _suggestionHeadersTemplatePropertyKey = DependencyProperty.RegisterReadOnly(nameof(AutoCompleteControlBase.SuggestionHeadersTemplate), typeof(DataTemplate), typeof(AutoCompleteControlBase),
            new PropertyMetadata());

        private static readonly Dictionary<int, DataTemplate> _suggestionHeadersTemplates = new Dictionary<int, DataTemplate>();

        private static readonly DependencyPropertyKey _suggestionsPropertyKey = DependencyProperty.RegisterReadOnly(nameof(AutoCompleteControlBase.Suggestions), typeof(ReadOnlyCollection<Suggestion>), typeof(AutoCompleteControlBase),
            new PropertyMetadata());

        private static readonly DependencyPropertyKey _suggestionTemplatePropertyKey = DependencyProperty.RegisterReadOnly(nameof(AutoCompleteControlBase.SuggestionTemplate), typeof(DataTemplate), typeof(AutoCompleteControlBase),
            new PropertyMetadata());

        private static readonly Dictionary<int, DataTemplate> _suggestionTemplates = new Dictionary<int, DataTemplate>();

        private readonly NestableScopeCreator _busyScopeCreator;
        private readonly ReaderWriterLockSlim _cancellationTokenSourceLock = new ReaderWriterLockSlim();

        // Copy of dependency property value for threaded access.
        private readonly List<string> _columnNames = new List<string>();

        private readonly IKeySearchParameters _keySearchParameters;
        private readonly Dictionary<string, object> _keyValues = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
        private readonly ReaderWriterLockSlim _keyValuesLock = new ReaderWriterLockSlim();
        private readonly ISuggestionParameters _suggestionParameters;
        private readonly NestableScopeCreator _suppressSuggestionScopeCreator = new NestableScopeCreator();
        private readonly NestableScopeCreator _suppressTextChangedScopeCreator = new NestableScopeCreator();
        private ColumnDataProjection _columnDataProjection;
        private Popup _dropDownPopup;

        // Copy of dependency property value for threaded access.
        private IEnumerable _itemsSource;

        // Copy of dependency property value for threaded access.
        private string _keyColumnName;

        // Copy of dependency property value for threaded access.
        private IKeySearchProvider _keySearchProvider = (IKeySearchProvider)AutoCompleteControlBase.KeySearchProviderProperty.DefaultMetadata.DefaultValue;

        // Copy of dependency property value for threaded access.
        private int _maxSuggestions = (int)AutoCompleteControlBase.MaxSuggestionsProperty.DefaultMetadata.DefaultValue;

        // Copy of dependency property value for threaded access.
        private MultiWordMatchType _multiWordMatchType;

        private SuggestionItemsControl _suggestionItemsControl;

        // Copy of dependency property value for threaded access.
        private ISuggestionProvider _suggestionProvider = (ISuggestionProvider)AutoCompleteControlBase.SuggestionProviderProperty.DefaultMetadata.DefaultValue;

        private Popup _suggestionsPopup;

        // Copy of dependency property value for threaded access.
        private TextMatchType _textMatchType;

        static AutoCompleteControlBase()
        {
            AutoCompleteControlBase.ColumnNamesProperty = AutoCompleteControlBase._columnNamesPropertyKey.DependencyProperty;
            AutoCompleteControlBase.IsBusyProperty = AutoCompleteControlBase._isBusyPropertyKey.DependencyProperty;
            AutoCompleteControlBase.GridViewProperty = AutoCompleteControlBase._gridViewPropertyKey.DependencyProperty;
            AutoCompleteControlBase.SuggestionHeadersTemplateProperty = AutoCompleteControlBase._suggestionHeadersTemplatePropertyKey.DependencyProperty;
            AutoCompleteControlBase.SuggestionsProperty = AutoCompleteControlBase._suggestionsPropertyKey.DependencyProperty;
            AutoCompleteControlBase.SuggestionTemplateProperty = AutoCompleteControlBase._suggestionTemplatePropertyKey.DependencyProperty;
            EventManager.RegisterClassHandler(typeof(SuggestionControl), UIElement.PreviewMouseLeftButtonUpEvent, new RoutedEventHandler(AutoCompleteControlBase.SuggestionControl_PreviewMouseLeftButtonUp));
        }

        protected AutoCompleteControlBase()
        {
            (this.ColumnNames = new ObservableCollection<string>()).CollectionChanged += this.Columns_CollectionChanged;
            this._columnDataProjection = this.ColumnDataProvider.DefaultProjection;
            this._keySearchParameters = new KeySearchParameters(this);
            this._suggestionParameters = new SuggestionParameters(this);
            this._busyScopeCreator = new NestableScopeCreator(() => this.SetBusy(true), () => this.SetBusy(false));
            this.CreateViews();
        }

        public event RoutedEventHandler TextChanged
        {
            add => this.AddHandler(AutoCompleteControlBase.TextChangedEvent, value);
            remove => this.RemoveHandler(AutoCompleteControlBase.TextChangedEvent, value);
        }

        public IColumnDataProvider ColumnDataProvider
        {
            get => (IColumnDataProvider)this.GetValue(AutoCompleteControlBase.ColumnDataProviderProperty);
            set => this.SetValue(AutoCompleteControlBase.ColumnDataProviderProperty, value);
        }

        public ObservableCollection<string> ColumnNames
        {
            get => (ObservableCollection<string>)this.GetValue(AutoCompleteControlBase.ColumnNamesProperty);
            private set => this.SetValue(AutoCompleteControlBase._columnNamesPropertyKey, value);
        }

        public string DisplayColumnName
        {
            get => (string)this.GetValue(AutoCompleteControlBase.DisplayColumnNameProperty);
            set => this.SetValue(AutoCompleteControlBase.DisplayColumnNameProperty, value);
        }

        public GridView GridView
        {
            get => (GridView)this.GetValue(AutoCompleteControlBase.GridViewProperty);
            private set => this.SetValue(AutoCompleteControlBase._gridViewPropertyKey, value);
        }

        public bool IsBusy
        {
            get => (bool)this.GetValue(AutoCompleteControlBase.IsBusyProperty);
            private set => this.SetValue(AutoCompleteControlBase._isBusyPropertyKey, BooleanBoxes.GetBox(value));
        }

        public bool IsDropDownOpen
        {
            get => (bool)this.GetValue(AutoCompleteControlBase.IsDropDownOpenProperty);
            set => this.SetValue(AutoCompleteControlBase.IsDropDownOpenProperty, BooleanBoxes.GetBox(value));
        }

        public bool IsReadOnly
        {
            get => (bool)this.GetValue(AutoCompleteControlBase.IsReadOnlyProperty);
            set => this.SetValue(AutoCompleteControlBase.IsReadOnlyProperty, BooleanBoxes.GetBox(value));
        }

        public IEnumerable ItemsSource
        {
            get => (IEnumerable)this.GetValue(AutoCompleteControlBase.ItemsSourceProperty);
            set => this.SetValue(AutoCompleteControlBase.ItemsSourceProperty, value);
        }

        public string KeyColumnName
        {
            get => (string)this.GetValue(AutoCompleteControlBase.KeyColumnNameProperty);
            set => this.SetValue(AutoCompleteControlBase.KeyColumnNameProperty, value);
        }

        public IKeySearchProvider KeySearchProvider
        {
            get => (IKeySearchProvider)this.GetValue(AutoCompleteControlBase.KeySearchProviderProperty);
            set => this.SetValue(AutoCompleteControlBase.KeySearchProviderProperty, value);
        }

        public int MaxSuggestions
        {
            get => (int)this.GetValue(AutoCompleteControlBase.MaxSuggestionsProperty);
            set => this.SetValue(AutoCompleteControlBase.MaxSuggestionsProperty, value);
        }

        public MultiWordMatchType MultiWordMatchType
        {
            get => (MultiWordMatchType)this.GetValue(AutoCompleteControlBase.MultiWordMatchTypeProperty);
            set => this.SetValue(AutoCompleteControlBase.MultiWordMatchTypeProperty, value);
        }

        public bool ShowColumnHeaders
        {
            get => (bool)this.GetValue(AutoCompleteControlBase.ShowColumnHeadersProperty);
            set => this.SetValue(AutoCompleteControlBase.ShowColumnHeadersProperty, BooleanBoxes.GetBox(value));
        }

        public DataTemplate SuggestionHeadersTemplate
        {
            get => (DataTemplate)this.GetValue(AutoCompleteControlBase.SuggestionHeadersTemplateProperty);
            private set => this.SetValue(AutoCompleteControlBase._suggestionHeadersTemplatePropertyKey, value);
        }

        public ISuggestionProvider SuggestionProvider
        {
            get => (ISuggestionProvider)this.GetValue(AutoCompleteControlBase.SuggestionProviderProperty);
            set => this.SetValue(AutoCompleteControlBase.SuggestionProviderProperty, value);
        }

        public ReadOnlyCollection<Suggestion> Suggestions
        {
            get => (ReadOnlyCollection<Suggestion>)this.GetValue(AutoCompleteControlBase.SuggestionsProperty);
            private set => this.SetValue(AutoCompleteControlBase._suggestionsPropertyKey, value);
        }

        public Brush SuggestionsBackground
        {
            get => (Brush)this.GetValue(AutoCompleteControlBase.SuggestionsBackgroundProperty);
            set => this.SetValue(AutoCompleteControlBase.SuggestionsBackgroundProperty, value);
        }

        public DataTemplate SuggestionTemplate
        {
            get => (DataTemplate)this.GetValue(AutoCompleteControlBase.SuggestionTemplateProperty);
            private set => this.SetValue(AutoCompleteControlBase._suggestionTemplatePropertyKey, value);
        }

        public string Text
        {
            get => (string)this.GetValue(AutoCompleteControlBase.TextProperty);
            set => this.SetValue(AutoCompleteControlBase.TextProperty, value);
        }

        public TextMatchType TextMatchType
        {
            get => (TextMatchType)this.GetValue(AutoCompleteControlBase.TextMatchTypeProperty);
            set => this.SetValue(AutoCompleteControlBase.TextMatchTypeProperty, value);
        }

        protected bool IsSuppressingSuggestions => this._suppressSuggestionScopeCreator.HasActiveScope;

        protected bool IsSuppressingTextChanged => this._suppressTextChangedScopeCreator.HasActiveScope;

        protected ListView ListView
        {
            get;
            private set;
        }

        protected TextBoxBase TextBox
        {
            get;
            private set;
        }

        public override void OnApplyTemplate()
        {
            if (this.TextBox != null)
            {
                this.TextBox.PreviewKeyDown -= this.TextBox_PreviewKeyDown;
            }
            if (this._suggestionsPopup != null)
            {
                this._suggestionsPopup.Opened -= this.SuggestionsPopup_Opened;
            }
            base.OnApplyTemplate();
            if ((this.TextBox = this.Template.FindName("PART_TextBox", this) as TextBoxBase) == null ||
                (this.ListView = this.Template.FindName("PART_ListView", this) as ListView) == null ||
                (this._suggestionsPopup = this.Template.FindName("PART_SuggestionsPopup", this) as Popup) == null ||
                (this._dropDownPopup = this.Template.FindName("PART_DropDownPopup", this) as Popup) == null ||
                (this._suggestionItemsControl = this.Template.FindName("PART_SuggestionItemsControl", this) as SuggestionItemsControl) == null)
            {
                throw new InvalidOperationException("Attempt to apply template without required template parts");
            }
            this.TextBox.PreviewKeyDown += this.TextBox_PreviewKeyDown;
            this._suggestionsPopup.Opened += this.SuggestionsPopup_Opened;
        }

        protected void CloseSuggestionsPopup()
        {
            this.Suggestions = null;
            if (this._suggestionsPopup != null)
            {
                this._suggestionsPopup.IsOpen = false;
            }
        }

        protected IDisposable CreateBusyScope() => this._busyScopeCreator.CreateScope();

        protected abstract string GetSuggestionSearchText();

        protected string GetTextColumnValue(object item, string columnName) => this._columnDataProjection.GetTextColumnValue(item, columnName);

        protected virtual void OnDisplayColumnNameChanged(string newValue)
        {
        }

        protected override void OnGotFocus(RoutedEventArgs e)
        {
            if (this.TextBox != null)
            {
                this.SetFocusToTextBox();
                e.Handled = true;
            }
            base.OnGotFocus(e);
        }

        protected virtual void OnIsDropDownOpenChanged(bool newValue)
        {
            if (!newValue)
            {
                if (this._suggestionsPopup != null)
                {
                    this._suggestionsPopup.IsOpen = false;
                }
                if (this.ListView != null)
                {
                    this.ListView.SelectionChanged -= this.ListView_SelectionChanged;
                }
                this.ReleaseMouseCapture();
            }
            else
            {
                this.CloseSuggestionsPopup();
                this.Dispatcher.BeginInvoke(DispatcherPriority.Render, new Action(this.OnDropDownOpened));
            }
        }

        protected virtual void OnItemSelected(object selectedItem)
        {
            this.IsDropDownOpen = false;
            this.CloseSuggestionsPopup();
        }

        protected virtual void OnItemsSourceChanged(IEnumerable newValue)
        {
            if (newValue != null)
            {
                this._columnDataProjection = this.ColumnDataProvider.CreateProjection(this._itemsSource = newValue);
            }
            else
            {
                this.IsDropDownOpen = false;
                this._columnDataProjection = this.ColumnDataProvider.DefaultProjection;
                this._itemsSource = null;
            }
            this.ClearKeyValuesDictionary();
        }

        protected virtual void OnKeyColumnNameChanged(string newValue)
        {
            this._keyColumnName = newValue;
            this.ClearKeyValuesDictionary();
        }

        protected virtual void OnKeySearchProviderChanged(IKeySearchProvider newValue)
        {
            this._keySearchProvider = newValue;
            this.ClearKeyValuesDictionary();
        }

        protected virtual void OnMaxSuggestionsChanged(int newValue)
        {
            this._maxSuggestions = newValue;
            this.CloseSuggestionsPopup();
        }

        protected virtual void OnMultiWordMatchTypeChanged(MultiWordMatchType newValue)
        {
            this._multiWordMatchType = newValue;
            this.CloseSuggestionsPopup();
        }

        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Up:
                    e.Handled = this.ProcessUpOrDownKey(true);
                    break;
                case Key.Down:
                    e.Handled = this.ProcessUpOrDownKey(false);
                    break;
                case Key.Escape:
                    this.IsDropDownOpen = false;
                    this.CloseSuggestionsPopup();
                    this.TextBox.Focus();
                    break;
                case Key.Enter:
                    this.IsDropDownOpen = false;
                    if (this._suggestionsPopup.IsOpen && this._suggestionItemsControl.ActiveContainer != null)
                    {
                        this.OnItemSelected(((Suggestion)this._suggestionItemsControl.ItemContainerGenerator.ItemFromContainer(this._suggestionItemsControl.ActiveContainer)).Item);
                        e.Handled = true;
                    }
                    break;
                case Key.Tab:
                    this.IsDropDownOpen = false;
                    this.CloseSuggestionsPopup();
                    break;
            }
            base.OnPreviewKeyDown(e);
        }

        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            if (this.IsDropDownOpen && !AutoCompleteControlBase.IsMouseInElement((this._dropDownPopup.Child as Decorator)?.Child ?? this._dropDownPopup.Child, e))
            {
                this.IsDropDownOpen = false;
                this.TextBox.Focus();
                e.Handled = true;
            }
            base.OnPreviewMouseDown(e);
        }

        protected virtual void OnSuggestionProviderChanged(ISuggestionProvider newValue)
        {
            this._suggestionProvider = newValue;
            this.CloseSuggestionsPopup();
        }

        protected virtual void OnTextBoxPreviewKeyDown(KeyEventArgs e)
        {
            if (e.Key != Key.Down || this.ItemsSource == null || this.IsDropDownOpen)
            {
                return;
            }
            this.IsDropDownOpen = true;
            e.Handled = true;
        }

        protected virtual void OnTextChanged(string newValue)
        {
            this.CloseSuggestionsPopup();
            this.RaiseEvent(new RoutedEventArgs(AutoCompleteControlBase.TextChangedEvent));
        }

        protected virtual void OnTextMatchTypeChanged(TextMatchType newValue)
        {
            this._textMatchType = newValue;
            this.CloseSuggestionsPopup();
        }

        protected abstract void ResetListViewSelection();

        protected Task<object> SearchByKey(string searchText)
        {
            this.Dispatcher.VerifyAccess();
            TaskCompletionSource<object> source = new TaskCompletionSource<object>();
            if (string.IsNullOrEmpty(searchText = searchText?.Trim()))
            {
                source.TrySetResult(null);
                return source.Task;
            }
            try
            {
                this._keyValuesLock.EnterReadLock();
                if (this._keyValues.TryGetValue(searchText, out object item))
                {
                    source.TrySetResult(item);
                    return source.Task;
                }
            }
            finally
            {
                this._keyValuesLock.ExitReadLock();
            }
            this._keySearchProvider.FindItemByKey(this._keySearchParameters, searchText, CancellationToken.None).ContinueWith(
                task =>
                {
                    if (task.IsFaulted && task.Exception != null)
                    {
                        source.TrySetException(task.Exception.InnerException);
                    }
                    else if (task.IsCanceled)
                    {
                        source.TrySetCanceled();
                    }
                    else
                    {
                        try
                        {
                            this._keyValuesLock.EnterWriteLock();
                            this._keyValues[searchText] = task.Result;
                        }
                        finally
                        {
                            this._keyValuesLock.ExitWriteLock();
                        }
                        source.TrySetResult(task.Result);
                    }
                },
                TaskContinuationOptions.ExecuteSynchronously
            );
            return source.Task;
        }

        protected void SearchForSuggestions()
        {
            this.Dispatcher.VerifyAccess();
            this.CloseSuggestionsPopup();
            string searchText = this.GetSuggestionSearchText();
            if (!this.IsLoaded || searchText == null || searchText.Length < this.SuggestionProvider.MinCharacters)
            {
                return;
            }
            TaskCompletionSource<ICollection<SuggestionData>> taskCompletionSource = new TaskCompletionSource<ICollection<SuggestionData>>();
            CancellationTokenSource tokenSource = new CancellationTokenSource();
            this._suggestionProvider.GetSuggestions(this._suggestionParameters, searchText, tokenSource.Token).ContinueWith(
                task =>
                {
                    if (task.IsFaulted && task.Exception != null)
                    {
                        taskCompletionSource.TrySetException(task.Exception.InnerException);
                    }
                    else if (tokenSource.IsCancellationRequested || task.IsCanceled)
                    {
                        taskCompletionSource.TrySetCanceled();
                    }
                    else if (task.Result == null)
                    {
                        taskCompletionSource.TrySetResult(null);
                    }
                    else
                    {
                        // This block serves two purposes.  It ensures there aren't more suggestions than appropriate, and it prevents the return of a lazy evaluated collection.
                        taskCompletionSource.TrySetResult(
                            task.Result is ICollection<SuggestionData> collection && collection.Count <= this._maxSuggestions
                                ? collection
                                : task.Result.Take(this._maxSuggestions).ToList()
                        );
                    }
                },
                TaskContinuationOptions.ExecuteSynchronously
            );
            taskCompletionSource.Task.ContinueWith(
                task =>
                {
                    if (tokenSource.IsCancellationRequested || task.IsCanceled || task.IsFaulted || task.Result == null || task.Result.Count == 0)
                    {
                        return;
                    }
                    if (!searchText.Equals(this.GetSuggestionSearchText()))
                    {
                        // Ensure that search text hasn't changed if it was a long running operation.
                        return;
                    }
                    Suggestion[] suggestions = new Suggestion[task.Result.Count];
                    using (IEnumerator<SuggestionData> enumerator = task.Result.GetEnumerator())
                    {
                        for (int index = 0; enumerator.MoveNext(); index++)
                        {
                            suggestions[index] = new Suggestion(enumerator.Current) { IsActive = index == 0 };
                        }
                    }
                    this.Suggestions = Array.AsReadOnly(suggestions);
                    this._suggestionsPopup.IsOpen = true;
                },
                CancellationToken.None,
                TaskContinuationOptions.None,
                TaskScheduler.FromCurrentSynchronizationContext()
            );
        }

        protected virtual void SetFocusToTextBox()
        {
            if (this.IsLoaded)
            {
                this.TextBox.Focus();
            }
        }

        protected IDisposable SuppressSuggestions() => this._suppressSuggestionScopeCreator.CreateScope();

        protected IDisposable SuppressTextChanged() => this._suppressTextChangedScopeCreator.CreateScope();

        private static object CoerceColumnDataProvider(DependencyObject d, object value)
        {
            return AutoCompleteControlBase.CoerceDependencyPropertyToDefault(AutoCompleteControlBase.ColumnDataProviderProperty, value);
        }

        private static object CoerceDependencyPropertyToDefault(DependencyProperty property, object value)
        {
            return value ?? property.DefaultMetadata.DefaultValue;
        }

        private static object CoerceIsDropDownOpen(DependencyObject d, object value)
        {
            AutoCompleteControlBase control = (AutoCompleteControlBase)d;
            if (control.IsLoaded && control.ItemsSource != null || !(bool)value)
            {
                return value;
            }
            if (!control.IsLoaded)
            {
                control.Loaded += AutoCompleteControlBase.OpenDropDownOnLoad;
            }
            return BooleanBoxes.FalseBox;
        }

        private static object CoerceKeySearchProvider(DependencyObject d, object value)
        {
            return AutoCompleteControlBase.CoerceDependencyPropertyToDefault(AutoCompleteControlBase.KeySearchProviderProperty, value);
        }

        private static object CoerceMaxSuggestions(DependencyObject d, object value)
        {
            return Math.Max(1, Math.Min(100, (int)value));
        }

        private static object CoerceSuggestionProvider(DependencyObject d, object value)
        {
            return AutoCompleteControlBase.CoerceDependencyPropertyToDefault(AutoCompleteControlBase.SuggestionProviderProperty, value);
        }

        private static void ColumnDataProvider_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            AutoCompleteControlBase control = (AutoCompleteControlBase)d;
            IColumnDataProvider provider = (IColumnDataProvider)e.NewValue;
            control._columnDataProjection = control.ItemsSource != null ? provider.CreateProjection(control.ItemsSource) : provider.DefaultProjection;
            control.ClearKeyValuesDictionary();
        }

        private static void DisplayColumnName_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((AutoCompleteControlBase)d).OnDisplayColumnNameChanged((string)e.NewValue);
        }

        private static DataTemplate GetSuggestionHeadersTemplate(int count)
        {
            if (count == 0)
            {
                return null;
            }
            DataTemplate dataTemplate;
            if (AutoCompleteControlBase._suggestionHeadersTemplates.TryGetValue(count, out dataTemplate))
            {
                return dataTemplate;
            }
            FrameworkElementFactory gridFactory = new FrameworkElementFactory(typeof(Grid));
            for (int index = 0; index < count; index++)
            {
                FrameworkElementFactory columnFactory = new FrameworkElementFactory(typeof(ColumnDefinition));
                columnFactory.SetValue(DefinitionBase.SharedSizeGroupProperty, "G" + index);
                gridFactory.AppendChild(columnFactory);
                FrameworkElementFactory textBlockFactory = new FrameworkElementFactory(typeof(TextBlock));
                textBlockFactory.SetValue(TextBlock.FontWeightProperty, FontWeights.SemiBold);
                textBlockFactory.SetValue(FrameworkElement.MarginProperty, new Thickness(4d, 0d, 4d, 0d));
                if (index != 0)
                {
                    textBlockFactory.SetValue(Grid.ColumnProperty, index);
                }
                textBlockFactory.SetBinding(TextBlock.TextProperty, new Binding(string.Concat("[", index.ToString(), "]")));
                gridFactory.AppendChild(textBlockFactory);
            }
            (dataTemplate = new DataTemplate { VisualTree = gridFactory }).Seal();
            AutoCompleteControlBase._suggestionHeadersTemplates.Add(count, dataTemplate);
            return dataTemplate;
        }

        private static DataTemplate GetSuggestionTemplate(int count)
        {
            if (count == 0)
            {
                return null;
            }
            if (AutoCompleteControlBase._suggestionTemplates.TryGetValue(count, out DataTemplate dataTemplate))
            {
                return dataTemplate;
            }
            FrameworkElementFactory gridFactory = new FrameworkElementFactory(typeof(Grid));
            for (int index = 0; index < count; index++)
            {
                FrameworkElementFactory columnFactory = new FrameworkElementFactory(typeof(ColumnDefinition));
                columnFactory.SetValue(DefinitionBase.SharedSizeGroupProperty, "G" + index);
                gridFactory.AppendChild(columnFactory);
                FrameworkElementFactory cellControlFactory = new FrameworkElementFactory(typeof(SuggestionCellControl));
                cellControlFactory.SetValue(FrameworkElement.MarginProperty, new Thickness(4d, 0d, 4d, 0d));
                cellControlFactory.SetBinding(SuggestionCellControl.CellProperty, new Binding(string.Concat("Cells[", index.ToString(), "]")));
                if (index != 0)
                {
                    cellControlFactory.SetValue(Grid.ColumnProperty, index);
                }
                gridFactory.AppendChild(cellControlFactory);
            }
            (dataTemplate = new DataTemplate { VisualTree = gridFactory }).Seal();
            AutoCompleteControlBase._suggestionTemplates.Add(count, dataTemplate);
            return dataTemplate;
        }

        private static void IsDropDownOpen_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((AutoCompleteControlBase)d).OnIsDropDownOpenChanged((bool)e.NewValue);
        }

        private static bool IsMouseInElement(UIElement element, MouseEventArgs args)
        {
            return VisualTreeHelper.GetDescendantBounds(element).Contains(args.GetPosition(element));
        }

        private static void ItemsSource_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((AutoCompleteControlBase)d).OnItemsSourceChanged((IEnumerable)e.NewValue);
        }

        private static void KeyColumnName_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((AutoCompleteControlBase)d).OnKeyColumnNameChanged((string)e.NewValue);
        }

        private static void KeySearchProvider_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((AutoCompleteControlBase)d).OnKeySearchProviderChanged((IKeySearchProvider)e.NewValue);
        }

        private static void MaxSuggestions_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((AutoCompleteControlBase)d).OnMaxSuggestionsChanged((int)e.NewValue);
        }

        private static void MultiWordMatchType_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((AutoCompleteControlBase)d).OnMultiWordMatchTypeChanged((MultiWordMatchType)e.NewValue);
        }

        private static void OpenDropDownOnLoad(object sender, RoutedEventArgs e)
        {
            AutoCompleteControlBase control = (AutoCompleteControlBase)sender;
            control.Loaded -= AutoCompleteControlBase.OpenDropDownOnLoad;
            control.IsDropDownOpen = control.ItemsSource != null;
        }

        private static void SuggestionControl_PreviewMouseLeftButtonUp(object sender, RoutedEventArgs e)
        {
            DependencyObject container = (DependencyObject)sender;
            SuggestionItemsControl itemsControl = (SuggestionItemsControl)ItemsControl.ItemsControlFromItemContainer(container);
            Suggestion suggestion = (Suggestion)itemsControl.ItemContainerGenerator.ItemFromContainer(container);
            itemsControl.Owner.OnItemSelected(suggestion.Item);
        }

        private static void SuggestionProvider_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((AutoCompleteControlBase)d).OnSuggestionProviderChanged((ISuggestionProvider)e.NewValue);
        }

        private static void Text_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((AutoCompleteControlBase)d).OnTextChanged((string)e.NewValue);
        }

        private static void TextMatchType_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((AutoCompleteControlBase)d).OnTextMatchTypeChanged((TextMatchType)e.NewValue);
        }

        private void ClearKeyValuesDictionary()
        {
            try
            {
                this._keyValuesLock.EnterUpgradeableReadLock();
                if (this._keyValues.Count == 0)
                {
                    return;
                }
                try
                {
                    this._keyValuesLock.EnterWriteLock();
                    this._keyValues.Clear();
                }
                finally
                {
                    this._keyValuesLock.ExitWriteLock();
                }
            }
            finally
            {
                this._keyValuesLock.ExitUpgradeableReadLock();
            }
        }

        private void Columns_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    this._columnNames.Insert(e.NewStartingIndex, (string)e.NewItems[0]);
                    break;
                case NotifyCollectionChangedAction.Remove:
                    this._columnNames.RemoveAt(e.OldStartingIndex);
                    break;
                case NotifyCollectionChangedAction.Replace:
                    this._columnNames[e.OldStartingIndex] = (string)e.NewItems[0];
                    break;
                case NotifyCollectionChangedAction.Move:
                    this._columnNames.RemoveAt(e.OldStartingIndex);
                    this._columnNames.Insert(e.NewStartingIndex, (string)e.OldItems[0]);
                    break;
                case NotifyCollectionChangedAction.Reset:
                    this._columnNames.Clear();
                    this._columnNames.AddRange((IEnumerable<string>)sender);
                    break;
            }
            this.CreateViews();
        }

        private GridView CreateGridView(IEnumerable<string> columnNames)
        {
            GridView view = new GridView();
            foreach (string columnName in columnNames)
            {
                GridViewColumn column = new GridViewColumn { Header = columnName };
                FrameworkElementFactory factory = new FrameworkElementFactory(typeof(TextBlock));
                factory.SetBinding(TextBlock.TextProperty, new Binding
                {
                    Converter = ColumnDataValueConverter.Instance,
                    ConverterParameter = new Converter<object, string>(item => this.GetTextColumnValue(item, columnName))
                });
                (column.CellTemplate = new DataTemplate { VisualTree = factory }).Seal();
                view.Columns.Add(column);
            }
            return view;
        }

        private void CreateViews()
        {
            IList<string> columnNames = this._columnNames.Count != 0 ? (IList<string>)this._columnNames : AutoCompleteControlBase._defaultColumnNames;
            this.GridView = this.CreateGridView(columnNames);
            this.SuggestionHeadersTemplate = AutoCompleteControlBase.GetSuggestionHeadersTemplate(columnNames.Count);
            this.SuggestionTemplate = AutoCompleteControlBase.GetSuggestionTemplate(columnNames.Count);
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.OnItemSelected(e.AddedItems.Count == 0 ? null : e.AddedItems[0]);
        }

        private void OnDropDownOpened()
        {
            if (this.IsLoaded && !this.TextBox.IsFocused)
            {
                this.TextBox.Focus();
            }
            Mouse.Capture(this, CaptureMode.SubTree);
            this.ResetListViewSelection();
            if (this.ListView.SelectedItem != null)
            {
                this.Dispatcher.BeginInvoke(DispatcherPriority.Render, new Action<object>(this.ListView.ScrollIntoView), this.ListView.SelectedItem);
            }
            this.ListView.SelectionChanged += this.ListView_SelectionChanged;
        }

        private bool ProcessUpOrDownKey(bool isUp)
        {
            if (!this._suggestionsPopup.IsOpen)
            {
                return false;
            }
            int index;
            if (this._suggestionItemsControl.ActiveContainer == null)
            {
                index = isUp ? this.Suggestions.Count - 1 : 0;
            }
            else if ((index = this._suggestionItemsControl.ItemContainerGenerator.IndexFromContainer(this._suggestionItemsControl.ActiveContainer) + (isUp ? -1 : 1)) < 0)
            {
                index = this.Suggestions.Count - 1;
            }
            else if (index >= this.Suggestions.Count)
            {
                index = 0;
            }
            this.Suggestions[index].IsActive = true;
            return true;
        }

        private void SetBusy(bool value)
        {
            if (!this.Dispatcher.CheckAccess())
            {
                this.Dispatcher.BeginInvoke(DispatcherPriority.Background, new Action<bool>(this.SetBusy), BooleanBoxes.GetBox(value));
                return;
            }
            this.IsBusy = value;
        }

        private void SuggestionsPopup_Opened(object sender, EventArgs e)
        {
            this.IsDropDownOpen = false;
        }

        private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            this.OnTextBoxPreviewKeyDown(e);
        }

        private sealed class ColumnDataValueConverter : IValueConverter
        {
            public static readonly ColumnDataValueConverter Instance = new ColumnDataValueConverter();

            object IValueConverter.Convert(object value, Type targetType, object parameter, CultureInfo culture)
            {
                if (value == null || value == DependencyProperty.UnsetValue || value == DBNull.Value)
                {
                    return value;
                }
                Converter<object, string> function = parameter as Converter<object, string>;
                return function == null ? DependencyProperty.UnsetValue : function(value);
            }

            object IValueConverter.ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            {
                throw new NotImplementedException();
            }
        }

        private sealed class KeySearchParameters : SearchParametersBase, IKeySearchParameters
        {
            public KeySearchParameters(AutoCompleteControlBase control)
                : base(control)
            {
            }

            public string KeyColumnName => this.Control._keyColumnName;
        }

        private abstract class SearchParametersBase
        {
            protected SearchParametersBase(AutoCompleteControlBase control)
            {
                this.Control = control;
            }

            public ColumnDataProjection ColumnDataProjection => this.Control._columnDataProjection;

            public IEnumerable ItemsSource => this.Control._itemsSource;

            protected AutoCompleteControlBase Control
            {
                get;
            }
        }

        private sealed class SuggestionParameters : SearchParametersBase, ISuggestionParameters
        {
            public SuggestionParameters(AutoCompleteControlBase control)
                : base(control)
            {
                this.ColumnNames = control._columnNames.AsReadOnly();
            }

            public ReadOnlyCollection<string> ColumnNames
            {
                get;
            }

            public int MaxSuggestions => this.Control._maxSuggestions;

            public MultiWordMatchType MultiWordMatchType => this.Control._multiWordMatchType;

            public TextMatchType TextMatchType => this.Control._textMatchType;

            public SuggestionData CreateSuggestionData(object item, string searchText)
            {
                return this.TryCreateSuggestionData(item, searchText, false).GetValueOrDefault();
            }

            public string[] ProduceColumnValues(object item)
            {
                if (item == null)
                {
                    return null;
                }
                if (this.ColumnNames.Count == 0)
                {
                    return new[] { TextConversion.ConvertToString(item) };
                }
                string[] values = new string[this.ColumnNames.Count];
                for (int index = 0; index < values.Length; index++)
                {
                    string columnName = this.ColumnNames[index];
                    values[index] = this.Control.GetTextColumnValue(item, columnName);
                }
                return values;
            }

            public SuggestionData? TryCreateSuggestionData(object item, string searchText)
            {
                return this.TryCreateSuggestionData(item, searchText, false);
            }

            private SuggestionData? TryCreateSuggestionData(object item, string searchText, bool forceCreate)
            {
                if (item == null)
                {
                    return null;
                }
                string[] columnValues = this.ProduceColumnValues(item);
                if (columnValues.Length == 0)
                {
                    return new SuggestionData(item, Array.Empty<SuggestionCell>());
                }
                if (string.IsNullOrEmpty(searchText = searchText?.Trim()))
                {
                    return new SuggestionData(item, Array.ConvertAll(columnValues, columnValue => new SuggestionCell(columnValue)));
                }
                ICollection<string> tokens;
                if (this.MultiWordMatchType == MultiWordMatchType.Verbatim)
                {
                    tokens = new[] { searchText };
                }
                else
                {
                    tokens = new HashSet<string>();
                    int startIndex = 0;
                    int index;
                    while ((index = searchText.IndexOf(' ', startIndex)) != -1)
                    {
                        string token = searchText.Substring(startIndex, index - startIndex).Trim();
                        if (token.Length != 0)
                        {
                            tokens.Add(token);
                        }
                        startIndex = index + 1;
                    }
                    string finalToken = searchText.Substring(startIndex).Trim();
                    if (finalToken.Length != 0)
                    {
                        tokens.Add(finalToken);
                    }
                }
                SuggestionCell[] cells = new SuggestionCell[columnValues.Length];
                HashSet<string> matchedTokens = new HashSet<string>();
                List<MatchSegment> segments = new List<MatchSegment>();
                for (int index = 0; index < columnValues.Length; index++)
                {
                    string columnValue = columnValues[index];
                    if (string.IsNullOrEmpty(columnValue))
                    {
                        cells[index] = new SuggestionCell(columnValue);
                        continue;
                    }
                    segments.Clear();
                    foreach (string token in tokens)
                    {
                        int startIndex = this.TextMatchType switch
                        {
                            TextMatchType.StartsWith => columnValue.StartsWith(token, StringComparison.OrdinalIgnoreCase) ? 0 : -1,
                            TextMatchType.EndsWith => columnValue.EndsWith(token, StringComparison.OrdinalIgnoreCase) ? columnValue.Length - token.Length : -1,
                            _ => columnValue.IndexOf(token, StringComparison.OrdinalIgnoreCase),
                        };
                        if (startIndex == -1)
                        {
                            continue;
                        }
                        segments.Add(new MatchSegment(startIndex, token.Length));
                        matchedTokens.Add(token);
                    }
                    cells[index] = segments.Count == 0 ? new SuggestionCell(columnValue) : new SuggestionCell(columnValue, segments.ToArray());
                }
                return !forceCreate && (matchedTokens.Count == 0 || matchedTokens.Count != tokens.Count && this.MultiWordMatchType == MultiWordMatchType.All)
                    ? default(SuggestionData?)
                    : new SuggestionData(item, cells);
            }
        }
    }
}