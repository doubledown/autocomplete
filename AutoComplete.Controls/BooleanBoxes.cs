namespace AutoComplete.Controls
{
    public static class BooleanBoxes
    {
        public static readonly object FalseBox = false;

        public static readonly object TrueBox = true;

        public static object GetBox(bool value) => value ? BooleanBoxes.TrueBox : BooleanBoxes.FalseBox;
    }
}