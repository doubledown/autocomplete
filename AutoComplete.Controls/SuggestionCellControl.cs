﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace AutoComplete.Controls
{
    [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
    public sealed class SuggestionCellControl : Control
    {
        public static readonly DependencyProperty CellProperty = DependencyProperty.Register(nameof(SuggestionCellControl.Cell), typeof(SuggestionCell?), typeof(SuggestionCellControl),
            new PropertyMetadata(SuggestionCellControl.Cell_PropertyChanged));

        private static readonly Brush _brush = SuggestionCellControl.CreateBrush();

        private TextBlock _textBlock;

        static SuggestionCellControl()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SuggestionCellControl), new FrameworkPropertyMetadata(typeof(SuggestionCellControl)));
        }

        public SuggestionCell? Cell
        {
            get => (SuggestionCell?)this.GetValue(SuggestionCellControl.CellProperty);
            set => this.SetValue(SuggestionCellControl.CellProperty, value);
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this._textBlock = this.Template.FindName("PART_TextBlock", this) as TextBlock;
            this.ProcessCell();
        }

        private static void Cell_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((SuggestionCellControl)d).ProcessCell();
        }

        private static Brush CreateBrush()
        {
            Brush brush = new SolidColorBrush(new Color { A = 50, R = 0, G = 0, B = 0 });
            brush.Freeze();
            return brush;
        }

        private static IEnumerable<MatchBlock> Merge(IEnumerable<MatchSegment> segments)
        {
            using IEnumerator<MatchSegment> enumerator = segments.OrderBy(segment => segment.StartIndex).GetEnumerator();
            bool skipMoveNext = false;
            while (skipMoveNext || enumerator.MoveNext())
            {
                skipMoveNext = false;
                MatchBlock block = new MatchBlock(enumerator.Current);
                while (enumerator.MoveNext())
                {
                    MatchSegment segment = enumerator.Current;
                    if (segment.StartIndex > block.EndIndex)
                    {
                        skipMoveNext = true;
                        break;
                    }
                    block.Absorb(segment);
                }
                yield return block;
            }
        }

        private void ProcessCell()
        {
            if (this._textBlock == null)
            {
                return;
            }
            this._textBlock.Inlines.Clear();
            SuggestionCell? cell = this.Cell;
            if (string.IsNullOrEmpty(cell?.Value))
            {
                return;
            }
            string value = cell.Value.Value;
            switch (cell.Value.Segments.Count)
            {
                case 0:
                    this._textBlock.Inlines.Add(new Run(value));
                    return;
                case 1:
                    MatchSegment segment = cell.Value.Segments[0];
                    if (segment.StartIndex != 0)
                    {
                        this._textBlock.Inlines.Add(new Run(value.Substring(0, segment.StartIndex)));
                    }
                    this._textBlock.Inlines.Add(new Run(value.Substring(segment.StartIndex, segment.Length))
                    {
                        Background = SuggestionCellControl._brush
                    });
                    if (segment.EndIndex < value.Length)
                    {
                        this._textBlock.Inlines.Add(new Run(value.Substring(segment.EndIndex)));
                    }
                    return;
            }
            int startIndex = 0;
            foreach (MatchBlock block in SuggestionCellControl.Merge(cell.Value.Segments))
            {
                if (block.StartIndex > startIndex)
                {
                    this._textBlock.Inlines.Add(new Run(value.Substring(startIndex, block.StartIndex - startIndex)));
                }
                this._textBlock.Inlines.Add(new Run(value.Substring(block.StartIndex, block.Length))
                {
                    Background = SuggestionCellControl._brush
                });
                startIndex = block.EndIndex;
            }
            if (startIndex < value.Length)
            {
                this._textBlock.Inlines.Add(new Run(value.Substring(startIndex)));
            }
        }

        private sealed class MatchBlock
        {
            public MatchBlock(MatchSegment segment)
            {
                this.EndIndex = (this.StartIndex = segment.StartIndex) + segment.Length;
            }

            public int EndIndex
            {
                get;
                private set;
            }

            public int Length => this.EndIndex - this.StartIndex;

            public int StartIndex
            {
                get;
                private set;
            }

            public void Absorb(MatchSegment segment)
            {
                this.StartIndex = Math.Min(this.StartIndex, segment.StartIndex);
                this.EndIndex = Math.Max(this.EndIndex, segment.EndIndex);
            }
        }
    }
}