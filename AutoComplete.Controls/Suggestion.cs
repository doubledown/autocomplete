﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace AutoComplete.Controls
{
    [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
    public sealed class Suggestion
    {
        private readonly SuggestionData _data;
        private bool _isActive;

        internal Suggestion(SuggestionData data) => this._data = data;

        public event EventHandler IsActiveChanged;

        public ReadOnlyCollection<SuggestionCell> Cells => this._data.Cells;

        public bool IsActive
        {
            get
            {
                return this._isActive;
            }
            set
            {
                if (this._isActive == value)
                {
                    return;
                }
                this._isActive = value;
                this.IsActiveChanged?.Invoke(this, EventArgs.Empty);
            }
        }

        public object Item => this._data.Item;
    }
}