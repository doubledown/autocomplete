using System;
using System.Threading;

namespace AutoComplete.Controls
{
    /// <summary>
    /// A class implementing <see cref="T:System.IDisposable"/> where a delegate is invoked when the class is disposed.
    /// No exception is raised if the class is disposed of multiple times, but the delegate is invoked only the first time.
    /// </summary>
    internal sealed class Disposable : IDisposable
    {
        /// <summary>
        /// An empty <see cref="T:System.IDisposable"/>
        /// </summary>
        public static readonly Disposable Empty = new Disposable();

        private Action _action;

        /// <summary>
        /// Initializes a new instance of the <see cref="Disposable"/> class.
        /// </summary>
        public Disposable(Action action = null) => this._action = action;

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose() => Interlocked.Exchange(ref this._action, null)?.Invoke();
    }
}