﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace AutoComplete.Controls
{
    public readonly struct SuggestionCell : IEquatable<SuggestionCell>
    {
        public SuggestionCell(string value, params MatchSegment[] segments)
        {
            this.Value = value;
            this.Segments = Array.AsReadOnly(segments != null && segments.Length != 0 ? segments : Array.Empty<MatchSegment>());
        }

        public ReadOnlyCollection<MatchSegment> Segments
        {
            get;
        }

        public string Value
        {
            get;
        }

        public bool Equals(SuggestionCell other)
        {
            if (this.Value != other.Value)
            {
                return false;
            }
            return this.Segments == null 
                ? other.Segments == null 
                : other.Segments != null && this.Segments.SequenceEqual(other.Segments);
        }

        public override bool Equals(object obj)
        {
            return obj is SuggestionCell other && this.Equals(other);
        }

        public override int GetHashCode()
        {
            if (this.Segments == null)
            {
                return 0;
            }
            return this.Segments.Aggregate(this.Value?.GetHashCode() ?? 0, (code, segment) =>
            {
                unchecked
                {
                    return (code << 5) + code ^ segment.GetHashCode();
                }
            });
        }
    }
}