﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace AutoComplete.Controls
{
    public sealed class IndexerColumnDataProvider : IColumnDataProvider
    {
        public static readonly IndexerColumnDataProvider Instance = new IndexerColumnDataProvider();

        private static readonly ParameterExpression _columnNameParameter = Expression.Parameter(typeof(string), "columnName");
        private static readonly Dictionary<Type, PropertyInfo> _indexers = new Dictionary<Type, PropertyInfo>();
        private static readonly ParameterExpression _itemParameter = Expression.Parameter(typeof(object), "item");
        private static readonly Dictionary<Type, ColumnDataProjection> _projections = new Dictionary<Type, ColumnDataProjection>();

        private IndexerColumnDataProvider()
        {
        }

        public ColumnDataProjection DefaultProjection
        {
            get;
        } = IndexerColumnDataProvider.GetColumnValue;

        ColumnDataProjection IColumnDataProvider.CreateProjection(IEnumerable itemsSource)
        {
            if (itemsSource is ICollectionView view)
            {
                itemsSource = view.SourceCollection;
            }
            Type enumerableType = itemsSource.GetType().GetInterface(typeof(IEnumerable<>).FullName);
            Type elementType = enumerableType != null ? enumerableType.GetGenericArguments()[0] : itemsSource.Cast<object>().Select(item => item?.GetType()).FirstOrDefault(type => type != null);
            if (elementType == null)
            {
                return this.DefaultProjection;
            }
            if (IndexerColumnDataProvider._projections.TryGetValue(elementType, out ColumnDataProjection projection))
            {
                return projection;
            }
            Type dictionaryType = elementType.GetInterface(typeof(IDictionary<,>).FullName);
            Type[] genericArguments;
            Expression body;
            if (dictionaryType != null && ((genericArguments = dictionaryType.GetGenericArguments())[0] == typeof(string) || genericArguments[0] == typeof(object)))
            {
                ParameterExpression valueVariable = Expression.Variable(genericArguments[1]);
                body = Expression.Block(
                    new[] { valueVariable },
                    Expression.Condition(
                        Expression.Call(
                            Expression.Convert(
                                IndexerColumnDataProvider._itemParameter,
                                dictionaryType
                            ),
                            nameof(IDictionary<object,object>.TryGetValue),
                            Type.EmptyTypes,
                            IndexerColumnDataProvider._columnNameParameter,
                            valueVariable
                        ),
                        Expression.Convert(
                            valueVariable,
                            typeof(object)
                        ),
                        Expression.Default(
                            typeof(object)
                        )
                    )
                );
            }
            else
            {
                PropertyInfo indexer = IndexerColumnDataProvider.GetIndexer(elementType);
                if (indexer == null)
                {
                    IndexerColumnDataProvider._projections.Add(elementType, projection = IndexerColumnDataProvider.GetColumnValue);
                    return projection;
                }
                body = Expression.Convert(
                    Expression.Property(
                        Expression.Convert(
                            IndexerColumnDataProvider._itemParameter,
                            elementType
                        ),
                        indexer,
                        IndexerColumnDataProvider._columnNameParameter
                    ),
                    typeof(object)
                );
            }
            IndexerColumnDataProvider._projections.Add(
                elementType,
                projection = Expression.Lambda<ColumnDataProjection>(
                    body,
                    IndexerColumnDataProvider._itemParameter,
                    IndexerColumnDataProvider._columnNameParameter
                ).Compile()
            );
            return projection;
        }

        private static object GetColumnValue(object item, string columnName)
        {
            return IndexerColumnDataProvider.GetIndexer(item.GetType())?.GetValue(item, new object[] { columnName });
        }

        private static PropertyInfo GetIndexer(Type elementType)
        {
            if (!IndexerColumnDataProvider._indexers.TryGetValue(elementType, out PropertyInfo indexer))
            {
                IndexerColumnDataProvider._indexers.Add(
                    elementType,
                    indexer = (from property in elementType.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                               let getMethod = property.GetGetMethod()
                               where getMethod != null
                               let parameters = getMethod.GetParameters()
                               where parameters.Length == 1 && parameters[0].ParameterType == typeof(string)
                               select property).FirstOrDefault()
                );
            }
            return indexer;
        }
    }
}