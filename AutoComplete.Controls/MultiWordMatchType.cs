namespace AutoComplete.Controls
{
    /// <summary>
    /// Enumeration specifying how to match multiple words.
    /// </summary>
    public enum MultiWordMatchType
    {
        /// <summary>
        /// Match all words - split on whitespace and only match if it contains all words.
        /// </summary>
        All = 0,

        /// <summary>
        /// Match any word - split on whitespace and match if it contains any of the words.
        /// </summary>
        Any = 1,

        /// <summary>
        /// Match verbatim - do not split on whitespace.
        /// </summary>
        Verbatim = 2
    }
}