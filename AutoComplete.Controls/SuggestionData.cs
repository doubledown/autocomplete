using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace AutoComplete.Controls
{
    public readonly struct SuggestionData : IEquatable<SuggestionData>
    {
        public SuggestionData(object item, params SuggestionCell[] cells)
        {
            this.Item = item;
            this.Cells = Array.AsReadOnly(cells != null && cells.Length != 0 ? cells : Array.Empty<SuggestionCell>());
        }

        public ReadOnlyCollection<SuggestionCell> Cells
        {
            get;
        }

        public object Item
        {
            get;
        }

        public bool Equals(SuggestionData other)
        {
            if (!object.Equals(this.Item, other.Item))
            {
                return false;
            }
            if (this.Cells == null)
            {
                return other.Cells == null;
            }
            return other.Cells != null && this.Cells.SequenceEqual(other.Cells);
        }

        public override bool Equals(object obj) => obj is SuggestionData other && this.Equals(other);

        public override int GetHashCode()
        {
            if (this.Cells == null)
            {
                return 0;
            }
            return this.Cells.Aggregate(this.Item?.GetHashCode() ?? 0, (code, cell) =>
            {
                unchecked
                {
                    return (code << 5) + code ^ cell.GetHashCode();
                }
            });
        }
    }
}