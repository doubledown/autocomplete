using System;
using System.Collections;

namespace AutoComplete.Controls
{
    /// <summary>
    /// Static methods for Column Data Providers.
    /// </summary>
    public static class ColumnDataProviderMethods
    {
        /// <summary>
        /// A column data provider that retrieves column data via a <paramref name="projection"/>.
        /// </summary>
        /// <param name="projection">The projection.</param>
        /// <returns>Column data provider.</returns>
        public static IColumnDataProvider CreateProvider(ColumnDataProjection projection)
        {
            return new DelegateColumnDataProvider(projection ?? throw new ArgumentNullException(nameof(projection)));
        }

        private sealed class DelegateColumnDataProvider : IColumnDataProvider
        {
            private readonly ColumnDataProjection _projection;

            public DelegateColumnDataProvider(ColumnDataProjection projection) => this._projection = projection;

            ColumnDataProjection IColumnDataProvider.DefaultProjection => this._projection;

            ColumnDataProjection IColumnDataProvider.CreateProjection(IEnumerable itemsSource) => this._projection;
        }
    }
}