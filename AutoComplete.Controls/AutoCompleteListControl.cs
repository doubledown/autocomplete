﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Threading;

namespace AutoComplete.Controls
{
    public sealed class AutoCompleteListControl : AutoCompleteControlBase
    {
        public static readonly DependencyProperty ChipTemplateProperty = DependencyProperty.Register(nameof(AutoCompleteListControl.ChipTemplate), typeof(DataTemplate), typeof(AutoCompleteListControl),
            new PropertyMetadata());

        public static readonly DependencyProperty IsPopulatingProperty;

        public static readonly RoutedCommand RemoveChipCommand = new RoutedCommand(nameof(AutoCompleteListControl.RemoveChip), typeof(AutoCompleteListControl));

        public static readonly DependencyProperty SeparatorProperty = DependencyProperty.Register(nameof(AutoCompleteListControl.Separator), typeof(char), typeof(AutoCompleteListControl),
            new PropertyMetadata(',', AutoCompleteListControl.Separator_PropertyChanged));

        private static readonly DependencyPropertyKey _isPopulatingPropertyKey = DependencyProperty.RegisterReadOnly(nameof(AutoCompleteListControl.IsPopulating), typeof(bool), typeof(AutoCompleteListControl),
            new PropertyMetadata(BooleanBoxes.FalseBox));

        private readonly char[] _separator;
        private readonly char[] _trimCharacters;

        private bool _isCutOperation;
        private CancellationTokenSource _populationCancellationTokenSource;
        private RichTextBox _textBox;

        static AutoCompleteListControl()
        {
            AutoCompleteListControl.IsPopulatingProperty = AutoCompleteListControl._isPopulatingPropertyKey.DependencyProperty;
            CommandManager.RegisterClassCommandBinding(
                typeof(AutoCompleteListControl),
                new CommandBinding(AutoCompleteListControl.RemoveChipCommand, AutoCompleteListControl.RemoveChipCommand_Executed, AutoCompleteListControl.RemoveChipCommand_CanExecute)
            );
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(AutoCompleteListControl), new FrameworkPropertyMetadata(typeof(AutoCompleteListControl)));
        }

        public AutoCompleteListControl()
        {
            char separator = (char)AutoCompleteListControl.SeparatorProperty.DefaultMetadata.DefaultValue;
            this._separator = new[] { separator };
            this._trimCharacters = new[] { separator, ' ' };
        }

        public DataTemplate ChipTemplate
        {
            get => (DataTemplate)this.GetValue(AutoCompleteListControl.ChipTemplateProperty);
            set => this.SetValue(AutoCompleteListControl.ChipTemplateProperty, value);
        }

        public bool IsPopulating
        {
            get => (bool)this.GetValue(AutoCompleteListControl.IsPopulatingProperty);
            private set => this.SetValue(AutoCompleteListControl._isPopulatingPropertyKey, BooleanBoxes.GetBox(value));
        }

        public char Separator
        {
            get => (char)this.GetValue(AutoCompleteListControl.SeparatorProperty);
            set => this.SetValue(AutoCompleteListControl.SeparatorProperty, value);
        }

        public object[] GetTextData()
        {
            if (string.IsNullOrEmpty(this.Text))
            {
                return Array.Empty<object>();
            }
            if (this._textBox == null)
            {
                return this.Text.Split(this._separator, StringSplitOptions.RemoveEmptyEntries);
            }
            List<object> list = new List<object>();
            StringBuilder builder = new StringBuilder();
            foreach (Inline inline in this._textBox.Document.Blocks.OfType<Paragraph>().SelectMany(paragraph => paragraph.Inlines))
            {
                if (inline is Run run)
                {
                    builder.Append(run.Text);
                }
                else
                {
                    this.AppendTextData(list, builder);
                    list.Add(((ContentPresenter)((InlineUIContainer)inline).Child).Content);
                }
            }
            this.AppendTextData(list, builder);
            return list.ToArray();
        }

        public override void OnApplyTemplate()
        {
            if (this._textBox != null)
            {
                this._textBox.TextChanged -= this.TextBox_TextChanged;
                DataObject.RemovePastingHandler(this._textBox, this.TextBox_Pasting);
                DataObject.RemoveCopyingHandler(this._textBox, this.TextBox_Copying);
            }
            base.OnApplyTemplate();
            if ((this._textBox = this.TextBox as RichTextBox) == null)
            {
                throw new InvalidOperationException("Attempt to apply template without required template parts");
            }
            this._textBox.Document = new FlowDocument();
            DataObject.AddCopyingHandler(this._textBox, this.TextBox_Copying);
            DataObject.AddPastingHandler(this._textBox, this.TextBox_Pasting);
            this._textBox.TextChanged += this.TextBox_TextChanged;
            this.PopulateTextBox();
        }

        protected override string GetSuggestionSearchText()
        {
            if (this._textBox == null || !this._textBox.Selection.IsEmpty || this._textBox.Document.Blocks.Count == 0)
            {
                return null;
            }
            Run run = this._textBox.Document.Blocks
                .OfType<Paragraph>()
                .SelectMany(paragraph => paragraph.Inlines.OfType<Run>())
                .FirstOrDefault(current => this._textBox.CaretPosition.CompareTo(current.ContentStart) >= 0 && this._textBox.CaretPosition.CompareTo(current.ContentEnd) <= 0);
            if (run == null)
            {
                return null;
            }
            RunSegment segment = this.ComputeCurrentActiveStringSegment(run);
            return run.Text.Substring(segment.StartIndex, segment.EndIndex - segment.StartIndex).Trim(this._trimCharacters);
        }

        protected override void OnDisplayColumnNameChanged(string newValue)
        {
            base.OnDisplayColumnNameChanged(newValue);
            this.PopulateTextBox();
        }

        protected override void OnItemSelected(object selectedItem)
        {
            base.OnItemSelected(selectedItem);
            if (this._textBox == null)
            {
                return;
            }
            InlineUIContainer uiContainer = new InlineUIContainer(new ContentPresenter { Content = this.CreateChip(selectedItem) });
            Paragraph paragraph = this._textBox.CaretPosition.Paragraph;
            try
            {
                this._textBox.TextChanged -= this.TextBox_TextChanged;
                if (paragraph == null)
                {
                    this._textBox.Document.Blocks.Add(new Paragraph(uiContainer));
                    return;
                }
                if (!this._textBox.Selection.IsEmpty)
                {
                    this._textBox.Selection.Select(this._textBox.Selection.End, this._textBox.Selection.End);
                }
                Inline inline = paragraph.Inlines.FirstOrDefault(current => this._textBox.CaretPosition.CompareTo(current.ContentStart) >= 0 && this._textBox.CaretPosition.CompareTo(current.ContentEnd) <= 0);
                if (inline == null)
                {
                    if (this._textBox.CaretPosition.GetAdjacentElement(LogicalDirection.Backward) is Inline sibling)
                    {
                        paragraph.Inlines.InsertAfter(sibling, uiContainer);
                    }
                    else if ((sibling = this._textBox.CaretPosition.GetAdjacentElement(LogicalDirection.Forward) as Inline) != null)
                    {
                        paragraph.Inlines.InsertBefore(sibling, uiContainer);
                    }
                    else
                    {
                        paragraph.Inlines.Add(uiContainer);
                    }
                }
                else if (inline is Run run)
                {
                    RunSegment segment = this.ComputeCurrentActiveStringSegment(run);
                    if (segment.StartIndex != 0)
                    {
                        paragraph.Inlines.InsertBefore(run, new Run(run.Text.Substring(0, segment.StartIndex)));
                    }
                    paragraph.Inlines.InsertBefore(run, uiContainer);
                    if (segment.EndIndex != run.Text.Length)
                    {
                        string endText = run.Text.Substring(segment.EndIndex);
                        if (endText.Length != 0 && endText[0] == this._separator[0])
                        {
                            endText = endText.Substring(1);
                        }
                        if (endText.Length != 0)
                        {
                            paragraph.Inlines.InsertAfter(uiContainer, new Run(endText));
                        }
                    }
                    paragraph.Inlines.Remove(run);
                }
                else if (this._textBox.CaretPosition.CompareTo(inline.ContentStart) <= 0)
                {
                    paragraph.Inlines.InsertBefore(inline, uiContainer);
                }
                else
                {
                    paragraph.Inlines.InsertAfter(inline, uiContainer);
                }
            }
            finally
            {
                this._textBox.TextChanged += this.TextBox_TextChanged;
                this.RecalculateText();
                this.Dispatcher.BeginInvoke(
                    DispatcherPriority.Render,
                    new Action<TextElement>(element =>
                    {
                        this.SetFocusToTextBox();
                        this._textBox.CaretPosition = element.ContentEnd;
                    }),
                    uiContainer
                );
            }
        }

        protected override void OnItemsSourceChanged(IEnumerable newValue)
        {
            base.OnItemsSourceChanged(newValue);
            this.PopulateTextBox();
        }

        protected override void OnKeyColumnNameChanged(string newValue)
        {
            base.OnKeyColumnNameChanged(newValue);
            this.PopulateTextBox();
        }

        protected override void OnKeySearchProviderChanged(IKeySearchProvider newValue)
        {
            base.OnKeySearchProviderChanged(newValue);
            this.PopulateTextBox();
        }

        protected override void OnTextBoxPreviewKeyDown(KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.X:
                    if ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control && !this.IsReadOnly)
                    {
                        this._isCutOperation = true;
                    }
                    return;
            }
            base.OnTextBoxPreviewKeyDown(e);
        }

        protected override void OnTextChanged(string newValue)
        {
            base.OnTextChanged(newValue);
            if (this.IsSuppressingTextChanged)
            {
                return;
            }
            this.PopulateTextBox();
        }

        protected override void ResetListViewSelection()
        {
            this.ListView.SelectedIndex = -1;
        }

        private static void RemoveChipCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = e.Parameter is Chip;
        }

        private static void RemoveChipCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            ((AutoCompleteListControl)sender).RemoveChip((Chip)e.Parameter);
        }

        private static void Separator_PropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            AutoCompleteListControl control = (AutoCompleteListControl)d;
            control._separator[0] = control._trimCharacters[0] = (char)e.NewValue;
            control.PopulateTextBox();
        }

        private void AppendTextData(List<object> list, StringBuilder builder)
        {
            if (builder.Length == 0)
            {
                return;
            }
            list.AddRange(builder.ToString().Split(this._separator, StringSplitOptions.RemoveEmptyEntries).Select(token => token.Trim()).Where(token => token.Length != 0));
            builder.Clear();
        }

        private RunSegment ComputeCurrentActiveStringSegment(Run run)
        {
            int position = run.ContentStart.GetOffsetToPosition(this._textBox.CaretPosition);
            int startIndex, endIndex;
            if (position == run.Text.Length)
            {
                startIndex = run.Text.LastIndexOf(this._separator[0]) + 1;
                endIndex = position;
            }
            else if (run.Text[position] == this._separator[0])
            {
                startIndex = position != 0 ? 1 + run.Text.LastIndexOf(this._separator[0], position - 1) : 0;
                endIndex = position;
            }
            else
            {
                startIndex = position != 0 ? 1 + run.Text.LastIndexOf(this._separator[0], position) : 0;
                int next = run.Text.IndexOf(this._separator[0], position);
                endIndex = next != -1 ? 1 + next : run.Text.Length;
            }
            return new RunSegment(startIndex, endIndex);
        }

        private Chip CreateChip(object item)
        {
            return new Chip(item, this.GetTextColumnValue(item, this.DisplayColumnName), this.GetTextColumnValue(item, this.KeyColumnName));
        }

        private Task PerformKeySearch(CancellationTokenSource source, int index, string[] tokens, object[] items)
        {
            CancellationTokenSource other = Interlocked.Exchange(ref this._populationCancellationTokenSource, source);
            if (other != null && !other.Equals(source))
            {
                other.Cancel();
            }
            return this.SearchByKey(tokens[index] = tokens[index].Trim()).ContinueWith(
                searchTask =>
                {
                    if (!searchTask.IsFaulted && !searchTask.IsCanceled)
                    {
                        items[index] = searchTask.Result;
                    }
                    else
                    {
                        source.Cancel();
                    }
                    return index == tokens.Length - 1 || source.Token.IsCancellationRequested ? searchTask : this.PerformKeySearch(source, index + 1, tokens, items);
                },
                source.Token,
                TaskContinuationOptions.ExecuteSynchronously,
                TaskScheduler.FromCurrentSynchronizationContext()
            ).Unwrap();
        }

        private void PopulateTextBox()
        {
            if (this._textBox == null)
            {
                return;
            }
            if (string.IsNullOrEmpty(this.Text))
            {
                try
                {
                    this._textBox.TextChanged -= this.TextBox_TextChanged;
                    this._textBox.Document.Blocks.Clear();
                }
                finally
                {
                    this._textBox.TextChanged += this.TextBox_TextChanged;
                }
                return;
            }
            this.IsPopulating = true;
            CancellationTokenSource source = new CancellationTokenSource();
            string[] tokens = this.Text.Split(this._separator);
            object[] items = new object[tokens.Length];
            this.PerformKeySearch(source, 0, tokens, items).ContinueWith(task =>
            {
                try
                {
                    if (task.IsFaulted || source.IsCancellationRequested)
                    {
                        return;
                    }
                    try
                    {
                        this._textBox.TextChanged -= this.TextBox_TextChanged;
                        StringBuilder textBuilder = new StringBuilder();
                        StringBuilder runBuilder = new StringBuilder();
                        this._textBox.Document.Blocks.Clear();
                        Paragraph paragraph = new Paragraph();
                        for (int index = 0; index < tokens.Length; index++)
                        {
                            if (index != 0)
                            {
                                textBuilder.Append(this._separator[0]);
                            }
                            string token = tokens[index];
                            object item = items[index];
                            if (item == null)
                            {
                                if (runBuilder.Length != 0)
                                {
                                    runBuilder.Append(this._separator[0]);
                                }
                                runBuilder.Append(token);
                                textBuilder.Append(token);
                                continue;
                            }
                            if (runBuilder.Length != 0)
                            {
                                paragraph.Inlines.Add(new Run(runBuilder.Append(this._separator[0]).ToString()));
                                runBuilder.Clear();
                            }
                            Chip chip = this.CreateChip(item);
                            paragraph.Inlines.Add(new InlineUIContainer(new ContentPresenter { Content = chip }));
                            textBuilder.Append(chip.KeyValue);
                        }
                        if (runBuilder.Length != 0)
                        {
                            paragraph.Inlines.Add(new Run(runBuilder.ToString()));
                        }
                        this._textBox.Document.Blocks.Add(paragraph);
                        using (this.SuppressTextChanged())
                        {
                            this.Text = textBuilder.ToString();
                        }
                        this.SetFocusToTextBox();
                        this._textBox.CaretPosition = this._textBox.Document.ContentEnd;
                    }
                    finally
                    {
                        this._textBox.TextChanged += this.TextBox_TextChanged;
                    }
                }
                finally
                {
                    if (Interlocked.CompareExchange(ref this._populationCancellationTokenSource, null, source) == source)
                    {
                        this.IsPopulating = false;
                    }
                    source.Dispose();
                }
            }, TaskContinuationOptions.ExecuteSynchronously);
        }

        private void RecalculateText()
        {
            string text;
            if (this._textBox == null || this._textBox.Document.Blocks.Count == 0)
            {
                text = string.Empty;
            }
            else
            {
                StringBuilder builder = new StringBuilder();
                foreach (Paragraph paragraph in this._textBox.Document.Blocks.OfType<Paragraph>())
                {
                    foreach (Inline inline in paragraph.Inlines)
                    {
                        switch (inline)
                        {
                            case InlineUIContainer container:
                                Chip chip = (Chip)((ContentPresenter)container.Child).Content;
                                if (builder.Length != 0 && builder[builder.Length - 1] != this._separator[0])
                                {
                                    builder.Append(this._separator[0]);
                                }
                                builder.Append(chip.KeyValue);
                                break;
                            case Run run:
                                if ((run.PreviousInline == null || run.PreviousInline is InlineUIContainer) && builder.Length != 0 && builder[builder.Length - 1] != this._separator[0])
                                {
                                    builder.Append(this._separator[0]);
                                }
                                builder.Append(run.Text.Trim());
                                break;
                        }
                    }
                }
                string doubleSeparator = new string(this._separator[0], 2);
                string singleSeparator = new string(this._separator);
                int length;
                do
                {
                    length = builder.Length;
                    builder.Replace(doubleSeparator, singleSeparator);
                }
                while (builder.Length != length);
                text = builder.ToString().Trim(this._trimCharacters);
            }
            using (this.SuppressTextChanged())
            {
                this.Text = text;
            }
        }

        private void RemoveChip(Chip chip)
        {
            InlineUIContainer match = this._textBox?.Document.Blocks
                .OfType<Paragraph>()
                .SelectMany(paragraph => paragraph.Inlines.OfType<InlineUIContainer>())
                .FirstOrDefault(container => chip.Equals((Chip)((ContentPresenter)container.Child).Content));
            if (match == null)
            {
                return;
            }
            Inline next = match.NextInline;
            Inline previous = match.PreviousInline;
            Paragraph parent = (Paragraph)match.Parent;
            parent.Inlines.Remove(match);
            if (next is Run nextRun && nextRun.Text.Length != 0 && nextRun.Text[0] == this._separator[0] &&
                previous is Run previousRun && previousRun.Text.Length != 0 && previousRun.Text[previousRun.Text.Length - 1] == this._separator[0])
            {
                previousRun.Text = previousRun.Text.Substring(0, previousRun.Text.Length - 1);
            }
            if (next != null)
            {
                this._textBox.CaretPosition = next.ContentStart;
            }
            else if (previous != null)
            {
                this._textBox.CaretPosition = previous.ContentEnd;
            }
            this.RecalculateText();
        }

        private void TextBox_Copying(object sender, DataObjectCopyingEventArgs e)
        {
            e.CancelCommand();
            bool isCutOperation = this._isCutOperation;
            if (this._isCutOperation)
            {
                this._isCutOperation = false;
            }
            if (this._textBox.Selection.IsEmpty)
            {
                Clipboard.SetData(DataFormats.Text, string.Empty);
                return;
            }
            List<Inline> inlines = this._textBox.Document.Blocks
                .OfType<Paragraph>()
                .SelectMany(paragraph => paragraph.Inlines)
                .Where(inline => inline.ContentEnd.CompareTo(this._textBox.Selection.Start) > 0)
                .TakeWhile(inline => inline.ContentStart.CompareTo(this._textBox.Selection.End) < 0)
                .ToList();
            Dictionary<Run, RunSegment> runSegments = new Dictionary<Run, RunSegment>();
            StringBuilder builder = new StringBuilder();
            foreach (Inline inline in inlines)
            {
                switch (inline)
                {
                    case InlineUIContainer container:
                        if (builder.Length != 0)
                        {
                            builder.Append(this._separator[0]);
                        }
                        builder.Append(((Chip)((ContentPresenter)container.Child).Content).KeyValue);
                        break;
                    case Run run:
                        if (builder.Length != 0 && run.PreviousInline is InlineUIContainer)
                        {
                            builder.Append(this._separator[0]);
                        }
                        int startIndex = run.ContentStart.CompareTo(this._textBox.Selection.Start) >= 0 ? 0 : run.ContentStart.GetOffsetToPosition(this._textBox.Selection.Start);
                        int endIndex = run.ContentEnd.CompareTo(this._textBox.Selection.End) <= 0 ? run.Text.Length : run.ContentStart.GetOffsetToPosition(this._textBox.Selection.End);
                        runSegments.Add(run, new RunSegment(startIndex, endIndex));
                        builder.Append(run.Text.Substring(startIndex, endIndex - startIndex));
                        break;
                }
            }
            Clipboard.SetData(DataFormats.Text, builder.ToString());
            if (!isCutOperation)
            {
                return;
            }
            foreach (Inline inline in inlines)
            {
                if (inline is Run run && runSegments.TryGetValue(run, out RunSegment segment) && (segment.StartIndex != 0 || segment.EndIndex != run.Text.Length))
                {
                    string before = segment.StartIndex == 0 ? null : run.Text.Substring(0, segment.StartIndex);
                    string after = segment.EndIndex == run.Text.Length ? null : run.Text.Substring(segment.EndIndex);
                    run.Text = before + after;
                    this._textBox.CaretPosition = run.ContentStart.GetPositionAtOffset(segment.StartIndex);
                }
                else
                {
                    ((Paragraph)inline.Parent).Inlines.Remove(inline);
                }
            }
            this.RecalculateText();
        }

        private void TextBox_Pasting(object sender, DataObjectPastingEventArgs e)
        {
            e.CancelCommand();
            if (!e.SourceDataObject.GetDataPresent(DataFormats.Text))
            {
                return;
            }
            // Only take the first line of raw text from the Clipboard.
            string text;
            using (StringReader reader = new StringReader((string)e.SourceDataObject.GetData(DataFormats.Text)))
            {
                string line = reader.ReadLine();
                if (line == null || (text = line.Trim()).Length == 0)
                {
                    return;
                }
            }
            this.IsPopulating = true;
            CancellationTokenSource source = new CancellationTokenSource();
            string[] tokens = text.Split(this._separator);
            object[] items = new object[tokens.Length];
            this.PerformKeySearch(source, 0, tokens, items).ContinueWith(
                task =>
                {
                    try
                    {
                        if (task.IsFaulted || source.IsCancellationRequested)
                        {
                            return;
                        }
                        try
                        {
                            this._textBox.TextChanged -= this.TextBox_TextChanged;
                            if (!this._textBox.Selection.IsEmpty)
                            {
                                List<Inline> inlines = new List<Inline>();
                                Dictionary<Run, RunSegment> segments = new Dictionary<Run, RunSegment>();
                                int offset = this._textBox.Document.ContentEnd.GetOffsetToPosition(this._textBox.Selection.End);
                                foreach (Inline inline in this._textBox.Document.Blocks.OfType<Paragraph>().SelectMany(para => para.Inlines))
                                {
                                    if (inline.ContentEnd.CompareTo(this._textBox.Selection.Start) <= 0)
                                    {
                                        continue;
                                    }
                                    if (inline.ContentStart.CompareTo(this._textBox.Selection.End) > 0)
                                    {
                                        break;
                                    }
                                    inlines.Add(inline);
                                    if (inline.ContentStart.CompareTo(this._textBox.Selection.Start) >= 0 && inline.ContentEnd.CompareTo(this._textBox.Selection.End) <= 0)
                                    {
                                        continue;
                                    }
                                    if (inline is Run run)
                                    {
                                        int startIndex = run.ContentStart.CompareTo(this._textBox.Selection.Start) >= 0 ? 0 : run.ContentStart.GetOffsetToPosition(this._textBox.Selection.Start);
                                        int endIndex = run.ContentEnd.CompareTo(this._textBox.Selection.End) <= 0 ? run.Text.Length : run.ContentStart.GetOffsetToPosition(this._textBox.Selection.End);
                                        segments.Add(run, new RunSegment(startIndex, endIndex));
                                    }
                                }
                                foreach (Inline inline in inlines)
                                {
                                    if (inline is Run run && segments.TryGetValue(run, out RunSegment segment))
                                    {
                                        string before = segment.StartIndex == 0 ? null : run.Text.Substring(0, segment.StartIndex);
                                        string after = segment.EndIndex == run.Text.Length ? null : run.Text.Substring(segment.EndIndex);
                                        run.Text = before + after;
                                    }
                                    else
                                    {
                                        ((Paragraph)inline.Parent).Inlines.Remove(inline);
                                    }
                                }
                                this._textBox.CaretPosition = this._textBox.Document.ContentEnd.GetPositionAtOffset(offset) ?? this._textBox.Document.ContentEnd;
                            }
                            Paragraph paragraph = this._textBox.CaretPosition.Paragraph;
                            if (paragraph == null)
                            {
                                this._textBox.Document.Blocks.Add(paragraph = new Paragraph());
                                this._textBox.CaretPosition = paragraph.ContentStart;
                            }
                            for (int index = 0; index < tokens.Length; index++)
                            {
                                string token = tokens[index];
                                object item = items[index];
                                InlineUIContainer container = item == null ? null : new InlineUIContainer(new ContentPresenter { Content = this.CreateChip(item) });
                                Inline inline = paragraph.Inlines.FirstOrDefault(current => this._textBox.CaretPosition.CompareTo(current.ContentStart) >= 0 && this._textBox.CaretPosition.CompareTo(current.ContentEnd) <= 0);
                                if (inline == null)
                                {
                                    Inline previous = this._textBox.CaretPosition.GetAdjacentElement(LogicalDirection.Backward) as Inline;
                                    Inline next = this._textBox.CaretPosition.GetAdjacentElement(LogicalDirection.Forward) as Inline;
                                    if (container != null)
                                    {
                                        if (previous != null)
                                        {
                                            paragraph.Inlines.InsertAfter(previous, container);
                                        }
                                        else if (next != null)
                                        {
                                            paragraph.Inlines.InsertBefore(next, container);
                                        }
                                        else
                                        {
                                            paragraph.Inlines.Add(container);
                                        }
                                        this._textBox.CaretPosition = container.ContentEnd;
                                    }
                                    else if (previous is Run word)
                                    {
                                        word.Text += token;
                                        this._textBox.CaretPosition = word.ContentEnd;
                                    }
                                    else if ((word = next as Run) != null)
                                    {
                                        word.Text = token + word.Text;
                                        this._textBox.CaretPosition = word.ContentStart.GetPositionAtOffset(token.Length) ?? this._textBox.CaretPosition;
                                    }
                                    else
                                    {
                                        word = new Run(token);
                                        if (previous != null)
                                        {
                                            paragraph.Inlines.InsertAfter(previous, word);
                                        }
                                        else if (next != null)
                                        {
                                            paragraph.Inlines.InsertBefore(next, word);
                                        }
                                        else
                                        {
                                            paragraph.Inlines.Add(word);
                                        }
                                        this._textBox.CaretPosition = word.ContentEnd;
                                    }
                                }
                                else if (inline is Run run)
                                {
                                    int offset = run.ContentStart.GetOffsetToPosition(this._textBox.CaretPosition);
                                    string before = offset == 0 ? null : run.Text.Substring(0, offset);
                                    string after = offset == run.Text.Length ? null : run.Text.Substring(offset);
                                    if (container == null)
                                    {
                                        if (index > 0 && before != null && before[before.Length - 1] != this._separator[0])
                                        {
                                            token = new string(this._separator) + token;
                                        }
                                        if (index != tokens.Length - 1 && after != null && after[0] != this._separator[0])
                                        {
                                            token += new string(this._separator);
                                        }
                                        run.Text = string.Concat(before, token, after);
                                        this._textBox.CaretPosition = run.ContentStart.GetPositionAtOffset(offset + token.Length) ?? this._textBox.CaretPosition;
                                    }
                                    else
                                    {
                                        if (before != null)
                                        {
                                            run.Text = before;
                                            paragraph.Inlines.InsertAfter(run, container);
                                            if (after != null)
                                            {
                                                paragraph.Inlines.InsertAfter(run, new Run(after));
                                            }
                                        }
                                        else if (after != null)
                                        {
                                            run.Text = after;
                                            paragraph.Inlines.InsertBefore(run, container);
                                        }
                                        else
                                        {
                                            Inline next = run.NextInline;
                                            Inline previous = run.PreviousInline;
                                            paragraph.Inlines.Remove(run);
                                            if (previous != null)
                                            {
                                                paragraph.Inlines.InsertAfter(previous, container);
                                            }
                                            else if (next != null)
                                            {
                                                paragraph.Inlines.InsertBefore(next, container);
                                            }
                                            else
                                            {
                                                paragraph.Inlines.Add(container);
                                            }
                                        }
                                        this._textBox.CaretPosition = container.ContentEnd;
                                    }
                                }
                                else // if (inline is UIContainer)
                                {
                                    Inline inlineToInsert = (Inline)container ?? new Run(token);
                                    if (this._textBox.CaretPosition.CompareTo(inline.ContentStart) <= 0)
                                    {
                                        paragraph.Inlines.InsertBefore(inline, inlineToInsert);
                                    }
                                    else
                                    {
                                        paragraph.Inlines.InsertAfter(inline, inlineToInsert);
                                    }
                                    this._textBox.CaretPosition = inlineToInsert.ContentEnd;
                                }
                            }
                        }
                        finally
                        {
                            this._textBox.TextChanged += this.TextBox_TextChanged;
                        }
                    }
                    finally
                    {
                        if (Interlocked.CompareExchange(ref this._populationCancellationTokenSource, null, source) == source)
                        {
                            this.IsPopulating = false;
                            this.RecalculateText();
                        }
                        source.Dispose();
                    }
                },
                TaskContinuationOptions.ExecuteSynchronously
            );
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.RecalculateText();
            if (this._textBox.Selection.IsEmpty)
            {
                this.SearchForSuggestions();
            }
        }

        private readonly struct RunSegment : IEquatable<RunSegment>
        {
            public RunSegment(int startIndex, int endIndex)
            {
                this.StartIndex = startIndex;
                this.EndIndex = endIndex;
            }

            public int EndIndex
            {
                get;
            }

            public int StartIndex
            {
                get;
            }

            public bool Equals(RunSegment other)
            {
                return this.EndIndex == other.EndIndex && this.StartIndex == other.StartIndex;
            }

            public override bool Equals(object obj) => obj is RunSegment other && this.Equals(other);

            public override int GetHashCode()
            {
                unchecked
                {
                    return this.EndIndex * 397 ^ this.StartIndex;
                }
            }
        }
    }
}