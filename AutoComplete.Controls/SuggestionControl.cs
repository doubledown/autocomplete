using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace AutoComplete.Controls
{
    [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
    public sealed class SuggestionControl : ContentControl
    {
        public static readonly DependencyProperty IsActiveProperty = DependencyProperty.Register(nameof(SuggestionControl.IsActive), typeof(bool), typeof(SuggestionControl),
            new PropertyMetadata(BooleanBoxes.FalseBox));

        static SuggestionControl()
        {
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(typeof(SuggestionControl), new FrameworkPropertyMetadata(typeof(SuggestionControl)));
        }

        public bool IsActive
        {
            get => (bool)this.GetValue(SuggestionControl.IsActiveProperty);
            set => this.SetValue(SuggestionControl.IsActiveProperty, BooleanBoxes.GetBox(value));
        }
    }
}