using System.Collections;

namespace AutoComplete.Controls
{
    /// <summary>
    /// Interface for a column data provider.
    /// </summary>
    public interface IColumnDataProvider
    {
        /// <summary>
        /// The projection to use when there is no items source.
        /// </summary>
        ColumnDataProjection DefaultProjection
        {
            get;
        }

        /// <summary>
        /// Allows the column data provider to produce a delegate given the specified <paramref name="itemsSource"/>.
        /// </summary>
        /// <param name="itemsSource">The items source.</param>
        /// <returns>Column data projection.</returns>
        ColumnDataProjection CreateProjection(IEnumerable itemsSource);
    }
}