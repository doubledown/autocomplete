﻿namespace AutoComplete.Controls
{
    /// <summary>
    /// A projection to get column data for the specified <paramref name="item"/>.
    /// </summary>
    /// <param name="item">The item to get column data for.</param>
    /// <param name="columnName">The name of the column.</param>
    /// <returns>Column data.</returns>
    /// <remarks>This delegate should only be invoked via <see cref="ColumnDataProjectionMethods"/>.</remarks>
    public delegate object ColumnDataProjection(object item, string columnName);

    /// <summary>
    /// Extension methods for the <see cref="ColumnDataProjection"/>.
    /// </summary>
    public static class ColumnDataProjectionMethods
    {
        /// <summary>
        /// Given a column data <paramref name="projection"/>, an <paramref name="item"/> and a column name, get the column value (and convert it to string).  
        /// If the column name is self referencing, simply convert the <paramref name="item"/> to string.
        /// </summary>
        /// <param name="projection">The column data projection.</param>
        /// <param name="item">The item.</param>
        /// <param name="columnName">The name of the column.</param>
        /// <returns>The value of the column converted to text.</returns>
        public static string GetTextColumnValue(this ColumnDataProjection projection, object item, string columnName)
        {
            return TextConversion.ConvertToString(string.IsNullOrEmpty(columnName) || columnName == "." ? item : projection(item, columnName));
        }
    }
}