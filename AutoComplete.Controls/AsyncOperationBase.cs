using System;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;

namespace AutoComplete.Controls
{
    public interface IAsyncOperation<out TParameters, in TResult>
    {
        CancellationToken CancellationToken
        {
            get;
        }

        bool IsCancellationRequested
        {
            get;
        }

        TParameters Parameters
        {
            get;
        }

        string SearchText
        {
            get;
        }

        void SetCanceled();

        void SetFailed(Exception exception);

        void SetFailed(string failure);

        void SetResult(TResult result);
    }

    [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
    public abstract class AsyncOperationBase<TParameters, TResult> : IAsyncOperation<TParameters, TResult>
    {
        private readonly TaskCompletionSource<TResult> _taskCompletionSource;

        protected AsyncOperationBase(TParameters parameters, string searchText, CancellationToken cancellationToken, object state = null)
        {
            this.Parameters = parameters;
            this.SearchText = searchText;
            this.CancellationToken = cancellationToken;
            this._taskCompletionSource = new TaskCompletionSource<TResult>(state);
        }

        public CancellationToken CancellationToken
        {
            get;
        }

        public bool IsCancellationRequested => this.CancellationToken.IsCancellationRequested;

        public TParameters Parameters
        {
            get;
        }

        public string SearchText
        {
            get;
        }

        public Task<TResult> Task => this._taskCompletionSource.Task;

        public void SetCanceled() => this._taskCompletionSource.TrySetCanceled();

        public void SetFailed(Exception exception) => this._taskCompletionSource.TrySetException(exception);

        public void SetFailed(string failure) => this._taskCompletionSource.TrySetException(new ApplicationException(failure));

        public void SetResult(TResult result) => this._taskCompletionSource.TrySetResult(result);
    }
}
