﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace AutoComplete.Controls
{
    [Browsable(false), EditorBrowsable(EditorBrowsableState.Never)]
    public sealed class SuggestionItemsControl : ItemsControl
    {
        public static readonly DependencyProperty ActiveContainerProperty;

        public static readonly DependencyProperty OwnerProperty = DependencyProperty.Register(nameof(SuggestionItemsControl.Owner), typeof(AutoCompleteControlBase), typeof(SuggestionItemsControl),
            new PropertyMetadata());

        private static readonly DependencyPropertyKey _activeContainerPropertyKey = DependencyProperty.RegisterReadOnly(nameof(SuggestionItemsControl.ActiveContainer), typeof(SuggestionControl), typeof(SuggestionItemsControl),
            new PropertyMetadata());

        private static readonly DependencyPropertyDescriptor _contentPropertyDescriptor = DependencyPropertyDescriptor.FromProperty(ContentControl.ContentProperty, typeof(SuggestionControl));
        private static readonly DependencyPropertyDescriptor _isActivePropertyDescriptor = DependencyPropertyDescriptor.FromProperty(SuggestionControl.IsActiveProperty, typeof(SuggestionControl));

        static SuggestionItemsControl()
        {
            SuggestionItemsControl.ActiveContainerProperty = SuggestionItemsControl._activeContainerPropertyKey.DependencyProperty;
        }

        public SuggestionControl ActiveContainer
        {
            get => (SuggestionControl)this.GetValue(SuggestionItemsControl.ActiveContainerProperty);
            private set => this.SetValue(SuggestionItemsControl._activeContainerPropertyKey, value);
        }

        public AutoCompleteControlBase Owner
        {
            get => (AutoCompleteControlBase)this.GetValue(SuggestionItemsControl.OwnerProperty);
            set => this.SetValue(SuggestionItemsControl.OwnerProperty, value);
        }

        protected override void ClearContainerForItemOverride(DependencyObject element, object item)
        {
            base.ClearContainerForItemOverride(element, item);
            BindingOperations.ClearBinding(element, SuggestionControl.IsActiveProperty);
            SuggestionItemsControl._isActivePropertyDescriptor.RemoveValueChanged(element, this.SuggestionControl_IsActiveChanged);
            SuggestionItemsControl._contentPropertyDescriptor.RemoveValueChanged(element, SuggestionItemsControl.SuggestionControl_ContentChanged);
        }

        protected override DependencyObject GetContainerForItemOverride() => new SuggestionControl();

        protected override bool IsItemItsOwnContainerOverride(object item) => item is SuggestionControl;

        protected override void PrepareContainerForItemOverride(DependencyObject element, object item)
        {
            SuggestionItemsControl._contentPropertyDescriptor.AddValueChanged(element, SuggestionItemsControl.SuggestionControl_ContentChanged);
            SuggestionItemsControl._isActivePropertyDescriptor.AddValueChanged(element, this.SuggestionControl_IsActiveChanged);
            base.PrepareContainerForItemOverride(element, item);
            BindingOperations.SetBinding(element, SuggestionControl.IsActiveProperty, new Binding(nameof(Suggestion.IsActive)) { Source = item, Mode = BindingMode.TwoWay });
            this.ProcessActiveState((SuggestionControl)element);
        }

        private static void SuggestionControl_ContentChanged(object sender, EventArgs e) => ((SuggestionControl)sender).IsActive = false;

        private void ProcessActiveState(SuggestionControl container)
        {
            bool isActiveContainer = container.Equals(this.ActiveContainer);
            if (isActiveContainer && (!container.IsActive || this.ItemContainerGenerator.ItemFromContainer(container) == DependencyProperty.UnsetValue))
            {
                this.ActiveContainer = null;
                return;
            }
            if (isActiveContainer || !container.IsActive)
            {
                return;
            }
            SuggestionControl other = this.ActiveContainer;
            this.ActiveContainer = container;
            if (other != null)
            {
                other.IsActive = false;
            }
            container.BringIntoView();
        }

        private void SuggestionControl_IsActiveChanged(object sender, EventArgs e) => this.ProcessActiveState((SuggestionControl)sender);
    }
}