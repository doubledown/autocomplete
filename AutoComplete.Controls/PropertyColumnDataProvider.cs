using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace AutoComplete.Controls
{
    public sealed class PropertyColumnDataProvider : IColumnDataProvider
    {
        public static readonly PropertyColumnDataProvider Instance = new PropertyColumnDataProvider();

        private static readonly ParameterExpression _columnNameParameter = Expression.Parameter(typeof(string), "columnName");
        private static readonly ParameterExpression _itemParameter = Expression.Parameter(typeof(object), "item");
        private static readonly Dictionary<Type, ColumnDataProjection> _reflectionProjections = new Dictionary<Type, ColumnDataProjection>();
        private static readonly MethodInfo _stringEqualsMethod = typeof(string).GetMethod(nameof(string.Equals), BindingFlags.Public | BindingFlags.Static, null, new[] { typeof(string), typeof(string) }, null);

        private PropertyColumnDataProvider()
        {
        }

        public ColumnDataProjection DefaultProjection { get; } = PropertyColumnDataProvider.GetColumnValue;

        ColumnDataProjection IColumnDataProvider.CreateProjection(IEnumerable itemsSource)
        {
            if (itemsSource is ICollectionView view)
            {
                itemsSource = view.SourceCollection;
            }
            if (itemsSource is ITypedList typedList)
            {
                return PropertyColumnDataProvider.CreateProjection(typedList.GetItemProperties(null));
            }
            Type enumerableType = itemsSource.GetType().GetInterface(typeof(IEnumerable<>).FullName);
            if (enumerableType != null)
            {
                ColumnDataProjection reflectionProjection = PropertyColumnDataProvider.GetReflectionProjection(enumerableType.GetGenericArguments()[0]);
                if (reflectionProjection != null)
                {
                    return reflectionProjection;
                }
            }
            else
            {
                object element = itemsSource.Cast<object>().FirstOrDefault();
                if (element != null)
                {
                    return PropertyColumnDataProvider.GetReflectionProjection(element.GetType()) ?? PropertyColumnDataProvider.CreateProjection(TypeDescriptor.GetProperties(element));
                }
            }
            return this.DefaultProjection;
        }

        private static ColumnDataProjection CreateProjection(PropertyDescriptorCollection properties)
        {
            return (item, columnName) => PropertyColumnDataProvider.GetColumnValue(properties, item, columnName);
        }

        private static object GetColumnValue(object item, string columnName)
        {
            ColumnDataProjection reflectionProjection = PropertyColumnDataProvider.GetReflectionProjection(item.GetType());
            return reflectionProjection != null ? reflectionProjection(item, columnName) : PropertyColumnDataProvider.GetColumnValue(TypeDescriptor.GetProperties(item), item, columnName);
        }

        private static object GetColumnValue(PropertyDescriptorCollection properties, object item, string columnName)
        {
            return properties.Find(columnName, false)?.GetValue(item);
        }

        private static ColumnDataProjection GetReflectionProjection(Type elementType)
        {
            if (PropertyColumnDataProvider._reflectionProjections.TryGetValue(elementType, out ColumnDataProjection projection))
            {
                return projection;
            }
            if (elementType.IsDefined(typeof(TypeDescriptionProviderAttribute), true) || typeof(ICustomTypeDescriptor).IsAssignableFrom(elementType))
            {
                PropertyColumnDataProvider._reflectionProjections.Add(elementType, null);
                return null;
            }
            Expression<ColumnDataProjection> lambdaExpression = Expression.Lambda<ColumnDataProjection>(
                Expression.Switch(
                    PropertyColumnDataProvider._columnNameParameter,
                    Expression.Default(
                        typeof(object)
                    ),
                    PropertyColumnDataProvider._stringEqualsMethod,
                    from property in elementType.GetProperties(BindingFlags.Public | BindingFlags.Instance)
                    let getMethod = property.GetGetMethod()
                    where getMethod != null && getMethod.GetParameters().Length == 0
                    select Expression.SwitchCase(
                        Expression.Convert(
                            Expression.Property(
                                Expression.Convert(
                                    PropertyColumnDataProvider._itemParameter,
                                    elementType
                                ),
                                property
                            ),
                            typeof(object)
                        ),
                        Expression.Constant(
                            property.Name,
                            typeof(string)
                        )
                    )
                ),
                PropertyColumnDataProvider._itemParameter,
                PropertyColumnDataProvider._columnNameParameter
            );
            PropertyColumnDataProvider._reflectionProjections.Add(elementType, projection = lambdaExpression.Compile());
            return projection;
        }
    }
}