﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows.Threading;

namespace AutoComplete.Controls
{
    public sealed class DispatcherCollection : DispatcherObject, IList<object>, IList, INotifyCollectionChanged
    {
        private readonly List<object> _list;

        public DispatcherCollection() => this._list = new List<object>();

        public DispatcherCollection(int capacity) => this._list = new List<object>(capacity);

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        public int Count
        {
            get
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    return (int)this.Dispatcher.Invoke(DispatcherPriority.Normal, new Func<int>(() => this._list.Count));
                }
                return this._list.Count;
            }
        }

        bool ICollection.IsSynchronized => true;

        object ICollection.SyncRoot => this.Dispatcher;

        public bool IsFixedSize => false;

        public bool IsReadOnly => false;

        public object this[int index]
        {
            get
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    return this.Dispatcher.Invoke(DispatcherPriority.Normal, new Func<int, object>(idx => this._list[idx]), index);
                }
                return this._list[index];
            }
            set
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action<int, object>((idx, item) => this[idx] = item), index, value);
                    return;
                }
                object oldValue = this._list[index];
                this._list[index] = value;
                this.CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, value, oldValue, index));
            }
        }

        public void Add(object item)
        {
            if (!this.Dispatcher.CheckAccess())
            {
                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action<object>(this.Add), item);
                return;
            }
            this._list.Add(item);
            this.CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, this._list.Count - 1));
        }

        public void AddRange(IEnumerable<object> collection)
        {
            if (!this.Dispatcher.CheckAccess())
            {
                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action<IEnumerable<object>>(this._list.AddRange), collection);
                return;
            }
            if (collection == null)
            {
                return;
            }
            foreach (object item in collection)
            {
                this._list.Add(item);
                this.CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, this._list.Count - 1));
            }
        }

        public void Clear()
        {
            if (!this.Dispatcher.CheckAccess())
            {
                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(this.Clear));
                return;
            }
            if (this._list.Count == 0)
            {
                return;
            }
            this._list.Clear();
            this.CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        public bool Contains(object item)
        {
            if (!this.Dispatcher.CheckAccess())
            {
                return (bool)this.Dispatcher.Invoke(DispatcherPriority.Normal, new Func<object, bool>(this.Contains), item);
            }
            return this._list.Contains(item);
        }

        public void CopyTo(object[] array, int arrayIndex)
        {
            if (!this.Dispatcher.CheckAccess())
            {
                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action<object[], int>(this._list.CopyTo), array, arrayIndex);
                return;
            }
            this._list.CopyTo(array, arrayIndex);
        }

        public IEnumerator<object> GetEnumerator()
        {
            if (!this.Dispatcher.CheckAccess())
            {
                return ((IEnumerable<object>)this.Dispatcher.Invoke(DispatcherPriority.Normal, new Func<object[]>(this._list.ToArray))).GetEnumerator();
            }
            return this._list.GetEnumerator();
        }

        void ICollection.CopyTo(Array array, int index)
        {
            if (!this.Dispatcher.CheckAccess())
            {
                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action<Array, int>(((ICollection)this._list).CopyTo), array, index);
                return;
            }
            ((ICollection)this._list).CopyTo(array, index);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        int IList.Add(object value)
        {
            if (!this.Dispatcher.CheckAccess())
            {
                return (int)this.Dispatcher.Invoke(DispatcherPriority.Normal, new Func<object, int>(this.NonGenericAdd), value);
            }
            return this.NonGenericAdd(value);
        }

        void IList.Remove(object value)
        {
            if (!this.Dispatcher.CheckAccess())
            {
                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Func<object, bool>(this.Remove), value);
                return;
            }
            this.Remove(value);
        }

        public int IndexOf(object item)
        {
            if (!this.Dispatcher.CheckAccess())
            {
                return (int)this.Dispatcher.Invoke(DispatcherPriority.Normal, new Func<object, int>(this._list.IndexOf), item);
            }
            return this._list.IndexOf(item);
        }

        public void Insert(int index, object item)
        {
            if (!this.Dispatcher.CheckAccess())
            {
                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action<int, object>(this.Insert), index, item);
                return;
            }
            this._list.Insert(index, item);
            this.CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, index));
        }

        public bool Remove(object item)
        {
            if (!this.Dispatcher.CheckAccess())
            {
                return (bool)this.Dispatcher.Invoke(DispatcherPriority.Normal, new Func<object, bool>(this.Remove), item);
            }
            int index = this._list.IndexOf(item);
            if (index == -1)
            {
                return false;
            }
            this._list.RemoveAt(index);
            this.CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, index));
            return true;
        }

        public void RemoveAt(int index)
        {
            if (!this.Dispatcher.CheckAccess())
            {
                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action<int>(this.RemoveAt), index);
                return;
            }
            object item = this._list[index];
            this._list.RemoveAt(index);
            this.CollectionChanged?.Invoke(this, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, index));
        }

        private int NonGenericAdd(object item)
        {
            this.Add(item);
            return this._list.Count - 1;
        }
    }
}