﻿using System.Threading;
using System.Threading.Tasks;

namespace AutoComplete.Controls
{
    /// <summary>
    /// Interface for a class capable of finding an item by key for the AutoComplete controls.
    /// </summary>
    public interface IKeySearchProvider
    {
        /// <summary>
        /// Search for an item with a key matching the specified search text.
        /// </summary>
        /// <param name="parameters">The search parameters.</param>
        /// <param name="searchText">The text to search for an object with a matching key.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>Task which will return the matched object if found, otherwise <c>null</c>.</returns>
        Task<object> FindItemByKey(IKeySearchParameters parameters, string searchText, CancellationToken cancellationToken);
    }
}