﻿using System;
using System.Windows;

namespace AutoComplete.Controls
{
    public static class TextConversion
    {
        public static string ConvertToString(object value, IFormatProvider provider = null)
        {
            if (value == null || value == DBNull.Value || value == DependencyProperty.UnsetValue)
            {
                return null;
            }
            Type valueType = value.GetType();
            if (valueType.IsEnum)
            {
                return Enum.GetName(valueType, value);
            }
            return (Type.GetTypeCode(valueType)) switch
            {
                TypeCode.Boolean => ((bool)value).ToString(provider),
                TypeCode.Byte => ((byte)value).ToString(provider),
                TypeCode.Char => ((char)value).ToString(provider),
                TypeCode.DateTime => ((DateTime)value).ToString("dd-MMM-yyyy", provider),
                TypeCode.Decimal => ((decimal)value).ToString("#,##0.####", provider),
                TypeCode.Double => ((double)value).ToString("#,##0.####", provider),
                TypeCode.Int16 => ((short)value).ToString("#,##0", provider),
                TypeCode.Int32 => ((int)value).ToString("#,##0", provider),
                TypeCode.Int64 => ((long)value).ToString("#,##0", provider),
                TypeCode.SByte => ((sbyte)value).ToString(provider),
                TypeCode.Single => ((float)value).ToString("#,##0.####", provider),
                TypeCode.String => (string)value,
                TypeCode.UInt16 => ((ushort)value).ToString("#,##0", provider),
                TypeCode.UInt32 => ((uint)value).ToString("#,##0", provider),
                TypeCode.UInt64 => ((ulong)value).ToString("#,##0", provider),
                _ => (value as IFormattable)?.ToString(null, provider) ?? value.ToString(),
            };
        }
    }
}