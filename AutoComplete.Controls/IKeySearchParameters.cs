using System.Collections;

namespace AutoComplete.Controls
{
    /// <summary>
    /// Interface for the paramters of a key search.
    /// </summary>
    public interface IKeySearchParameters
    {
        /// <summary>
        /// Gets a projection to get column data for an item.
        /// </summary>
        /// <remarks>
        /// Use <see cref="ColumnDataProjectionMethods"/> to use the delegate the same way the control does internally.
        /// </remarks>
        ColumnDataProjection ColumnDataProjection
        {
            get;
        }

        /// <summary>
        /// Gets the items source if it exists, otherwise <c>null</c>.
        /// </summary>
        IEnumerable ItemsSource
        {
            get;
        }

        /// <summary>
        /// Gets the key column name.
        /// </summary>
        string KeyColumnName
        {
            get;
        }
    }
}