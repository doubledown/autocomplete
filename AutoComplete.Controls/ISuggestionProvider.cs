﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace AutoComplete.Controls
{
    /// <summary>
    /// Interface for a class capable of generating suggestions for the AutoComplete controls.
    /// </summary>
    public interface ISuggestionProvider
    {
        /// <summary>
        /// Gets the minimum number of characters that must be entered before suggestions are triggered.
        /// </summary>
        int MinCharacters
        {
            get;
        }

        /// <summary>
        /// Given a set of search parameters, create a task to get a collection of suggestions.
        /// </summary>
        /// <param name="parameters">The search parameters.</param>
        /// <param name="searchText">The search text.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>Task which returns a collection of suggestion data.</returns>
        Task<IEnumerable<SuggestionData>> GetSuggestions(ISuggestionParameters parameters, string searchText, CancellationToken cancellationToken);
    }
}