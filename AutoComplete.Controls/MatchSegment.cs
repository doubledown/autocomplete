using System;

namespace AutoComplete.Controls
{
    public readonly struct MatchSegment : IEquatable<MatchSegment>
    {
        public MatchSegment(int startIndex, int length)
        {
            this.StartIndex = startIndex;
            this.Length = length;
        }

        public int Length
        {
            get;
        }

        public int StartIndex
        {
            get;
        }

        public int EndIndex => this.StartIndex + this.Length;

        public bool Equals(MatchSegment other)
        {
            return this.Length == other.Length && this.StartIndex == other.StartIndex;
        }

        public override bool Equals(object obj) => obj is MatchSegment other && this.Equals(other);

        public override int GetHashCode()
        {
            unchecked
            {
                return (this.Length * 397) ^ this.StartIndex;
            }
        }
    }
}