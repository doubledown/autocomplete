using System.Collections;
using System.Collections.ObjectModel;

namespace AutoComplete.Controls
{
    /// <summary>
    /// Interface for the parameters for a suggestion provider.
    /// </summary>
    public interface ISuggestionParameters
    {
        /// <summary>
        /// Gets a projection to get column data for an item.
        /// </summary>
        /// <remarks>Use <see cref="ColumnDataProjectionMethods"/> to use the delegate the same way the control does internally.</remarks>
        ColumnDataProjection ColumnDataProjection
        {
            get;
        }

        /// <summary>
        /// Gets the set of column names.
        /// </summary>
        ReadOnlyCollection<string> ColumnNames
        {
            get;
        }

        /// <summary>
        /// Gets the items source if it exists, otherwise <c>null</c>.
        /// </summary>
        IEnumerable ItemsSource
        {
            get;
        }

        /// <summary>
        /// Gets the maximum number of suggestions to be returned.
        /// </summary>
        int MaxSuggestions
        {
            get;
        }

        /// <summary>
        /// Gets an enumeration value specifying how to match text.
        /// </summary>
        TextMatchType TextMatchType
        {
            get;
        }

        /// <summary>
        /// Gets an enumeration value specifying how to match multiple words.
        /// </summary>
        MultiWordMatchType MultiWordMatchType
        {
            get;
        }

        /// <summary>
        /// Helper method to produce column values for the specified <paramref name="item"/>.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>String array of column values.</returns>
        string[] ProduceColumnValues(object item);
        
        /// <summary>
        /// Ignoring matching rules, create suggestion data for the specified <paramref name="item"/>.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="searchText">The search text.</param>
        /// <returns>SuggestionData.</returns>
        SuggestionData CreateSuggestionData(object item, string searchText);

        /// <summary>
        /// Attempts to create suggestion data for the specified <paramref name="item"/>.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <param name="searchText">The search text.</param>
        /// <returns>SuggestionData (or <c>null</c>).</returns>
        SuggestionData? TryCreateSuggestionData(object item, string searchText);
    }
}