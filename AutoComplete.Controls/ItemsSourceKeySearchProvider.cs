using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AutoComplete.Controls
{
    /// <summary>
    /// A class capable of finding an item by key for the AutoComplete controls given their items source.
    /// </summary>
    public sealed class ItemsSourceKeySearchProvider : IKeySearchProvider
    {
        /// <summary>
        /// Gets the sole instance of the <see cref="ItemsSourceKeySearchProvider"/> class.
        /// </summary>
        public static readonly ItemsSourceKeySearchProvider Instance = new ItemsSourceKeySearchProvider();

        /// <summary>
        /// Initializes a new instance of <see cref="ItemsSourceKeySearchProvider"/> class.
        /// </summary>
        private ItemsSourceKeySearchProvider()
        {
        }

        /// <summary>
        /// Search for an item with a key matching the specified <paramref name="key"/>.
        /// </summary>
        /// <param name="parameters">The search parameters.</param>
        /// <param name="key">The key to match.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        /// <returns>The matched object if found, otherwise <c>null</c>.</returns>
        public Task<object> FindItemByKey(IKeySearchParameters parameters, string key, CancellationToken cancellationToken)
        {
            TaskCompletionSource<object> source = new TaskCompletionSource<object>();
            try
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    source.TrySetCanceled();
                }
                else if (string.IsNullOrEmpty(key))
                {
                    source.TrySetResult(null);
                }
                else if (parameters.ItemsSource == null)
                {
                    source.TrySetResult(key);
                }
                else
                {
                    source.TrySetResult(parameters.ItemsSource.Cast<object>().FirstOrDefault(item => item != null && key.Equals(parameters.ColumnDataProjection.GetTextColumnValue(item, parameters.KeyColumnName), StringComparison.OrdinalIgnoreCase)));
                }
            }
            catch (Exception e)
            {
                source.TrySetException(e);
            }
            return source.Task;
        }
    }
}